﻿#ifndef PERCEPTRONMULTICOUCHE_H
#define PERCEPTRONMULTICOUCHE_H

/** @file reseauneurone.h
 * @brief Contient la classe <b>ReseauNeurone</b>, le perceptron multi-couches.
 * @author DI TROIA J. , BONNIN P.
 * @version 1.0
 * @date 01 août 2018
 */

#include <iostream>

#include <Neurone/neurone.h>

using namespace std;

/**
 * @class ReseauNeurone
 * @brief La classe ReseauNeurone.
 *
 * Modélise un Réseau de neurone de type perceptron multi-couches.
 * Tous les neurones d'une même couche ont le même nombre d'entrées et de poids.
 * Toutes les couches de neurones possèdent le même nombre de neurones, sauf la couche de sortie.
 *
 * Astuce:
 * Si l'utilisateur souhaite ignorer un neurone, il suffit de mettre les poids des liens des neurones
 * de la couche suivante correspondant au neurone à ignorer à 0.
 *
 */
class ReseauNeurone
{
public:
  /**
   * @brief Constructeur par défaut.
   *
   * Initialise la varialbe <b>isSoftmax</b> à <b>false</b>.
   *
   */
  ReseauNeurone();
  /**
   * @brief Crée un réseau de neurones.
   *
   * Le réseau doit avoir autant de neurones de sortie que de sorties possible.
   *
   * @param nbPoidsEntree Désigne le nombre de poids pour chacun des neurones de la couche d'entrée.
   * @param fonctionActivation La fonction d'activation de <b>tous</b> les neurones du perceptron multi-couches.
   * @param fonctionActivationDerivee La dérivée de la fonction d'activation de <b>tous</b> les neurones du perceptron multi-couche..
   * @param nbNeuroneParCouche Désigne le nombre de neurones par couche.
   * @param nbCouchesCachees Quantité de couche cachée (sans compter la couche d'entrée donc) du réseau.
   * @param nbNeuroneSorties Nombre de neurones sur la couche de sortie.
   */
  ReseauNeurone(size_t nbPoidsEntree, ptrfa_t fonctionActivation=Logistique, ptrfa_t fonctionActivationDerivee=LogistiqueDerivee, size_t nbNeuroneParCouche=1, size_t nbCouchesCachees=1, size_t nbNeuroneSorties=1);

  /**
   * @brief Crée un réseau de neurones sur base des dimensions du tableau 3D <b>poidsCouches</b>.
   *
   * Les dimensions représentent:
   *    - La dimension 1 représente le nombre de couche.
   *    - La dimension 2 représente le nombre de neurones par couche.
   *    - La dimension 3 représente le nombre de poids par neurone.
   *
   * @param fonctionActivation La fonction d'activation des neurones du perceptron.
   * @param poidsCouches Les poids des neurones du perceptron.
   */
  ReseauNeurone(ptrfa_t fonctionActivation, vector<vector<vector<double>>> &poidsCouches);

  /**
    * @brief Le destructeur de la classe.
    */
  ~ReseauNeurone();

  /**
   * @brief Evalue les entrées fournies au réseau et fournit la sortie du réseau.
   *
   * Propage les entrées soumises au réseau vers la sortie.
   *
   * @param entrees Les entrées soumises au réseau.
   * @return Un vecteur avec les sorties des neurones de la couche de sortie. Il y a autant d'élément dans ce vecteur qu'il y a de neurones dans la couche de sortie.
   */
  //Un vecteur avec les sorties des neurones de la couche de sortie. Il y a autant d'élément dans ce vecteur qu'il y a de neurones dans la couche cachée
  vector<double>& Evaluer(vector<double> &entrees);
  /**
   * @brief Evalue les entrées fournies au réseau et fournit la sortie du réseau.
   *
   * Version <b>DEBUG</b>: Utilisé pour la vérification du gradient !
   *
   * @param entrees Les entrées soumises au réseau.
   * @param epsilon Le pas de la dérivée utilisé pour calculer le gradient de l'erreur.
   * @return Un vecteur avec les sorties des neurones de la couche de sortie. Il y a autant d'élément dans ce vecteur qu'il y a de neurones dans la couche de sortie.
   */
  vector<double>& Evaluer(vector<double> &entrees, double epsilon); // Version DEBUG

  /**
   * @brief Calcule les potentiels des neurones de la couche de sortie.
   *
   * Cette fonction est utilisée dans le cadre d'une couche de sortie utilisant la fonction Softmax.
   *
   * @param entrees Les entrées de la couche de sortie.
   * @param dst Le vecteur dans lequel les potentiels seront stockés.
   */
  void CalculerPotentielCoucheSortie(vector<double> &entrees, vector<double> &dst);
  /**
   * @brief Calcule les potentiels des neurones de la couche de sortie.
   *
   * Cette fonction est utilisée dans le cadre d'une couche de sortie utilisant la fonction Softmax.
   * Version <b>DEBUG</b>: Utilisé pour la vérification du gradient !
   *
   * @param entrees Les entrées de la couche de sortie.
   * @param dst Le vecteur dans lequel les potentiels seront stockés.
   * @param epsilon Le pas de la dérivée utilisé pour calculer le gradient de l'erreur.
   */
  void CalculerPotentielCoucheSortie(vector<double> &entrees, vector<double> &dst, double epsilon); // Version DEBUG

  /**
   * @brief Permet de retirer l'indice de la couche de sortie.
   * @return L'indice de la couche de sortie dans le tableau <b>sortiesReseau</b>.
   */
  size_t getIndiceCoucheSortie();
  /**
   * @brief Permet de récupérer la couche de sortie.
   * @return La couche de neurones de sortie.
   */
  vector<Neurone>& getCoucheSortie();

  /**
   * @brief Retourne un string contenant les différentes sorties du réseau.
   * @return Le string contenant les sorties du réseau de neurones.
   */
  string SortiesToString();
  /**
   * @brief Retourne un string contenant les différents poids du réseau.
   *
   * Le biais est le dernier des poids à être affiché.
   * Chaque neurone d'une couche est séparé par deux tabulations.
   * Chaque couche est séparée par un Carriage Return
   *
   * @return Le string contenant les poids du neurone.
   */
  string PoidsToString();

  vector<vector<Neurone>> couches; /*!< Les différentes couches de neurones composant le réseau. */
  // sorties du réseau pour toutes les couches
  vector<vector<double>>sortiesReseau; /*!< Les différentes sorties <i>y</i> des neurones du réseau. */
  // Est-ce que la dernière couche à comme fonction d'activation Softmax ?
  bool isSoftmax; /*!< Indique si la dernière couche de neurones utilise la fonction d'activation Softmax (<b>true</b>). */

private:

};

#endif // PERCEPTRONMULTICOUCHE_H
