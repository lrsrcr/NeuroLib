﻿#ifndef NEURONE_H
#define NEURONE_H

/** @file neurone.h
 * @brief Contient la classe <b>neurone</b>, base de tous les perceptrons.
 * @author DI TROIA J. , BONNIN P.
 * @version 1.0
 * @date 01 août 2018
 */

#include <vector>
#include <sstream>
#include <ctime>
#include <limits>
#include <random>

#include <Fonctions/fonctionactivation.h>

using namespace std;

/**
 * @class Neurone.
 * @brief La classe <b>Neurone</b>, représentant un perceptron simple.
 */
class Neurone
{
public:
  /**
   * @brief Constructeur par défaut.
   *
   * Initialise le biais du neurone à 1, le potentiel et la sortie à 0.
   *
   */
  Neurone();
  /**
   * @brief Constructeur initialisant la fonction d'activation et sa dérivée ainsi que les poids.
   *
   * En initialisant les poids, ce constructeur permet de créer un perceptron dont une personne souhaiterait étudier le comportement, ou recréer un perceptron à partir d'un fichier.
   *
   * @param poids Le vecteur de poids du perceptron.
   * @param fonctionActivation La fonction d'activation du perceptron.
   * @param fonctionActivationDerivee La dérivée de la fonction d'activation.
   */
  Neurone(vector<double> &poids, ptrfa_t fonctionActivation, ptrfa_t fonctionActivationDerivee=LogistiqueDerivee);
  /**
   * @brief Constructeur initialisant la fonction d'activation et sa dérivée et un certain nombre <b>nbPoids</b> de poids.
   *
   * Initialise les poids du neurone selon une distribution Gaussienne de moyenne \f$\mu\f$
   * et d'écart-type \f$ \sigma \f$ représentés respectivement par les variables statiques
   * <b>mu</b> et <b>sigma</b> de la classe <b>Neurone</b>.
   *
   * Par défaut, la moyenne vaut \f$ \mu = 0 \f$ et l'écart-type vaut \f$ \sigma = 10^{-3} \f$.
   *
   * @param nbPoids Le nombre de poids dont perceptron doit disposer.
   * @param fonctionActivation La fonction d'activation du perceptron.
   * @param fonctionActivationDerivee La dérivée de la fonction d'activation.
   */
  Neurone(int nbPoids, ptrfa_t fonctionActivation, ptrfa_t fonctionActivationDerivee=LogistiqueDerivee);
  /**
    * @brief Le destructeur du Neurone.
    */
  ~Neurone();

  /**
   * @brief Evalue les entrées fournies au neurones.
   *
   * Calcule dans un premier temps le potentiel \f$ \nu = \sum_{i=0}^N w_i x_i \f$.
   * Dans un second temps, calcule la sortie du neurone \f$ y = \phi(\nu) \f$, avec \f$\phi(\cdot)\f$ la fonction d'activation du neurone.
   *
   * @param entrees Les entrées \f$ \mathbf{x} \f$ soumises au neurone.
   * @return La sortie du neurone en fonction des entrées qui lui ont été soumises.
   */
  double Evaluer(const vector<double> &entrees);
  /**
   * @brief Evalue les entrées fournies au neurones.
   *
   * Version <b>DEBUG</b>: Utilisé pour la vérification du gradient !
   *
   * @param entrees Les entrées soumises au neurone.
   * @param epsilon Le pas de la dérivée.
   * @return La sortie du neurone en fonction des entrées qui lui ont été soumises.
   */
  double Evaluer(const vector<double> &entrees, double epsilon); // Version DEBUG

  /**
   * @brief Calcule le potentiel du neurone en fonction des entrées qui lui ont été soumises.
   *
   * Calcule le potentiel du neurone \f$ \nu = \sum_{i=0}^N w_i x_i \f$.
   *
   * Cette fonction est appelée par la fonction <b>Evaluer</b>. Elle n'est pas destinée à être appelée dans du code client.
   *
   * @param entrees Les entrées soumises au neurone.
   * @return Le potentiel du neurone
   */
  double CalculerPotentiel(const vector<double> &entrees);
  /**
   * @brief CalculerPotentiel
   *
   * Version <b>DEBUG</b>: Utilisé pour la vérification du gradient
   *
   * @param entrees
   * @param epsilon
   * @return
   */
  double CalculerPotentiel(const vector<double> &entrees, double epsilon); // Version DEBUG
  /**
   * @brief Calcule la dérivée de la fonction d'activation du neurone.
   * @return La valeur de la dérivée de la fonction d'activation avec le potentiel actuel du neurone.
   */
  double getDerivee();
  /**
   * @brief Retourne un string contenant les différents poids du neurone.
   *
   * Retourne un string contenant les différents poids du neurones. Le biais est le dernier à être affiché.
   *
   * @return Le string contenant les poids du neurone.
   */
  string PoidsToString();

  //paramètres du neurones
  vector<double> poids;  /*!< Les poids du neurone. */
  //potentiel et sortie du neurone
  double potentiel, /*!< Le potentiel \f$ \nu \f$ du neurone. */
  y, /*!< La sortie y du neurone. */
  biais; /*!< Le biais \f$ x_{0} \f$ du neurone. */

  ptrfa_t fonctionActivation; /*!< Le pointeur de la fonction d'activation du neurone. */
  ptrfa_t fonctionActivationDerivee; /*!< Le pointeur de la dérivée de la fonction d'activation du neurone. */

  static double sigma,  /*!< L'écart-type de la distribution Gaussienne initialisant les poids.  */
  mu; /*!< La moyenne de la distribution Gaussienne initialisant les poids. */

private:
  /**
   * @brief Constructeur privé initialisant la fonction d'activation et sa dérivée.
   *
   * Ne prends pas en compte la notion de poids du neurone.
   *
   * @param fonctionActivation La fonction d'activation du perceptron.
   * @param fonctionActivationDerivee La dérivée de la fonction d'activation.
   */
  Neurone(ptrfa_t fonctionActivation, ptrfa_t fonctionActivationDerivee=LogistiqueDerivee);
  /**
   * @brief Initialise les poids du neurone selon une distribution Gaussienne.
   *
   * Initialise les poids du neurone selon une distribution Gaussienne de moyenne <b>moyenne</b>
   * et d'écart-type <b>ecarttype</b> tous deux passés en paramètre.
   *
   * @param nbPoids Le noùbre de poids a initialiser.
   * @param moyenne La moyenne de la distribution Gaussienne.
   * @param ecarttype L'écart-type de la distribution Gaussienne.
   */
  void InitPoidsGaussien(size_t nbPoids, double moyenne, double ecarttype);
};

#endif // NEURONE_H

