﻿#include "neurone.h"

double Neurone::sigma = 1E-3;
double Neurone::mu = 0;

Neurone::Neurone()
{
  this->biais=1.0f;
  potentiel=0.0f;
  y=0.0f;
}

Neurone::Neurone(ptrfa_t fonctionActivation, ptrfa_t fonctionActivationDerivee)
: Neurone()
{
  this->fonctionActivation=fonctionActivation;
  this->fonctionActivationDerivee=fonctionActivationDerivee;
}

Neurone::Neurone(vector<double> &poids, ptrfa_t fonctionActivation, ptrfa_t fonctionActivationDerivee)
: Neurone(fonctionActivation, fonctionActivationDerivee)
{
  this->poids=poids;
}

Neurone::Neurone(int nbPoids, ptrfa_t fonctionActivation, ptrfa_t fonctionActivationDerivee)
: Neurone(fonctionActivation, fonctionActivationDerivee)
{
  InitPoidsGaussien(nbPoids, mu, sigma);
}

Neurone::~Neurone() { }

double Neurone::Evaluer(const vector<double> &entrees)
{
  potentiel=CalculerPotentiel(entrees);
  y=fonctionActivation(potentiel);
  if(isnan(potentiel) || isnan(y))
      y=NAN-1; // Eviter le dépassement de capacité du type double.
  return y;
}

double Neurone::CalculerPotentiel(const vector<double> &entrees)
{
  double tmp=0.0f;
  for(size_t i=0; i<entrees.size(); i++)
    tmp+=entrees[i]*poids[i];

  return (isnan(tmp+biais))? NAN-1 : tmp+biais;
}

double Neurone::Evaluer(const vector<double> &entrees, double epsilon)
{
  potentiel=CalculerPotentiel(entrees, epsilon);
  y=fonctionActivation(potentiel);
  if(isnan(potentiel) || isnan(y))
      y=NAN-1; // Eviter le dépassement de capacité du type double.
  return y;
}

double Neurone::CalculerPotentiel(const vector<double> &entrees, double epsilon)
{
  if(entrees.size()!=poids.size())
    return std::numeric_limits<double>::quiet_NaN();

  double tmp=0.0f;
  for(size_t i=0; i<entrees.size(); i++)
    tmp+=entrees[i]*(poids[i] + epsilon);

  return tmp+biais;
}

double Neurone::getDerivee()
{
  return fonctionActivationDerivee(potentiel);
}

string Neurone::PoidsToString()
{
  ostringstream strs;
    for(double p: poids) // Concatène les poids
      strs << p << '\t';

    strs << "biais: " << biais; // Ajoute le biais à la fin
    return strs.str(); // Renvoie le contenu du flux sous forme de string
}

void Neurone::InitPoidsGaussien(size_t nbPoids, double moyenne, double ecarttype)
{
  poids=vector<double>(nbPoids);
  random_device rd;
  mt19937 e2(rd());
  normal_distribution<double> dist(moyenne, ecarttype);

  /* Plus il y a de poids, plus la variances des poids augmentera
   * La variance d'une entrée peut-être normalisée si on multiplie les poids d'un facteur 1/sqrt(nbPoids)
   */
  for(size_t i=0; i<nbPoids; i++)
    poids[i] = dist(e2);

  //double facteur = sqrt(1.0/nbPoids); // Source:http://cs231n.github.io/neural-networks-2/#init § Calibrating the variances with 1/sqrt(n)
  //for(size_t i=0; i<nbPoids; i++)
    //poids[i] *= /*0.01 * */ facteur;
}
