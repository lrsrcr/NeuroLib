﻿#include "reseauneurone.h"

ReseauNeurone::ReseauNeurone()
{
  isSoftmax=false;
}

ReseauNeurone::ReseauNeurone(size_t nbPoidsEntree, ptrfa_t fonctionActivation, ptrfa_t fonctionActivationDerivee,size_t nbNeuroneParCouche, size_t nbCouchesCachees, size_t nbNeuroneSorties)
:ReseauNeurone()
{
  //Créer couche d'entrée
  vector<Neurone> neuronesTmp(nbNeuroneParCouche);
  for(size_t i=0; i<nbNeuroneParCouche; i++)
    neuronesTmp[i] = Neurone(nbPoidsEntree, fonctionActivation, fonctionActivationDerivee);

  if(neuronesTmp.size()>0)
    couches.push_back(neuronesTmp);

  // Créer toutes les couches cachées
  for(size_t i=0; i<nbCouchesCachees-1; i++) // Conversion en int obligatoire: si nbCouchesCachees vaut 0, alors nbCouchesCachees-1 vaut -1 -> pas dans size_t
  {
    // Créer tous les neurones pour chaque couche cachée
    neuronesTmp = vector<Neurone>(nbNeuroneParCouche);
    for(size_t j=0; j<nbNeuroneParCouche; j++)
      neuronesTmp[j]=Neurone(nbNeuroneParCouche, fonctionActivation, fonctionActivationDerivee);

    couches.push_back(neuronesTmp);
  }

  // Créer les neurones de sorties
  vector<Neurone> neuronesSorties(nbNeuroneSorties);
  for(size_t i=0; i<nbNeuroneSorties; i++)
    //Les entrées des neurones de sortie sont les neurones cachés et donc il y a autant d'entrées que de neurone cachées
    neuronesSorties[i] = Neurone(nbNeuroneParCouche, fonctionActivation, fonctionActivationDerivee);

  if(nbNeuroneSorties>0)
    couches.push_back(neuronesSorties);

  //Créer les tableaux de sorties des couches cachées
  size_t nbCouches=nbCouchesCachees+1;
  sortiesReseau=vector<vector<double>>(nbCouches);//nb de couches cachees + 1 seule couche de sortie
  for(size_t i=0; i<nbCouchesCachees; i++)
    sortiesReseau[i]=vector<double>(nbNeuroneParCouche);

  if(nbNeuroneSorties>0)
    sortiesReseau[nbCouches-1]=vector<double>(nbNeuroneSorties);
}

ReseauNeurone::ReseauNeurone(ptrfa_t fonctionActivation, vector<vector<vector<double>>> &poidsCouches)
{
  //poidsCouches[i][j][k]: kème poids du jème neurone de la couche i
  size_t nbCouches=poidsCouches.size();

  // Créer les couches du réseau
  size_t i;
  for(i=0; i<nbCouches; i++)
  {
    // Créer tous les neurones pour chaque couche cachée
    vector<Neurone> neuronesTmp;
    size_t nbNeuronesCouche=poidsCouches[i].size(); // Un vecteur<double> par neurone et autant de vecteur que de neurone
    for(size_t j=0; j<nbNeuronesCouche; j++)
      neuronesTmp.push_back(Neurone(poidsCouches[i][j], fonctionActivation));

    couches.push_back(neuronesTmp);
  }

  //Créer les tableaux de sorties des couches cachées
  sortiesReseau=vector<vector<double>>(nbCouches);//nb de couches cachees + 1 seule couche de sortie
  for(size_t i=0; i<nbCouches; i++)
    sortiesReseau[i]=vector<double>(poidsCouches[i].size());

  sortiesReseau[nbCouches-1]=vector<double>(poidsCouches[nbCouches-1].size());
}

ReseauNeurone::~ReseauNeurone() { }

vector<double>& ReseauNeurone::Evaluer(vector<double> &entrees)
{
  // couche d'entrée
  for(size_t j=0; j<couches[0].size(); j++)
    sortiesReseau[0][j]=couches[0][j].Evaluer(entrees);

  // couches cachées
  for(size_t i=1; i<couches.size()-1; i++)
    for(size_t j=0; j<couches[i].size(); j++)// La couche précédente sert d'entrées à la couche actuelle
      sortiesReseau[i][j]= couches[i][j].Evaluer(sortiesReseau[i-1]);

  // Couche de sortie
  vector<Neurone> &coucheSortie=getCoucheSortie();
  vector<double> &sortiesReseauSorties=sortiesReseau[getIndiceCoucheSortie()];

  if(!isSoftmax)
  {
    if(couches.size()>1)
      for(size_t i=0; i<coucheSortie.size(); i++) // Pour chacun des neurones de sortie
        sortiesReseauSorties[i]= coucheSortie[i].Evaluer(sortiesReseau[getIndiceCoucheSortie()-1]);
  }
  else // Calculer la probabilité de la réponse via Softmax
  {
    // Calculer la somme des exponentielles des différents potentiels des neurones
    vector<double> potentiels;
    CalculerPotentielCoucheSortie(sortiesReseau[getIndiceCoucheSortie()-1], potentiels);
    double sommeExponentiellePotentiels=CalculerSommeExponentielle(potentiels);

    if(isnan(sommeExponentiellePotentiels))
        cout << "Somme exponentielle is nan" << endl;

    for(size_t i=0; i<coucheSortie.size(); i++) // Pour chacun des neurones de sortie
    {
      double tmp = Softmax(potentiels[i], sommeExponentiellePotentiels);
      tmp = (tmp>=1 || isnan(tmp))? 0.99995 : tmp;
      sortiesReseauSorties[i] = tmp;
      coucheSortie[i].potentiel = potentiels[i]; // Copier résultats dans les neurones
      coucheSortie[i].y = sortiesReseauSorties[i];
    }
  }

  return sortiesReseau[getIndiceCoucheSortie()];
}

vector<double> &ReseauNeurone::Evaluer(vector<double> &entrees, double epsilon)
{
  // couche d'entrée
  for(size_t j=0; j<couches[0].size(); j++)
    sortiesReseau[0][j]=couches[0][j].Evaluer(entrees, epsilon);

  // couches cachées
  for(size_t i=1; i<couches.size()-1; i++)
    for(size_t j=0; j<couches[i].size(); j++)// La couche précédente sert d'entrées à la couche actuelle
      sortiesReseau[i][j]= couches[i][j].Evaluer(sortiesReseau[i-1], epsilon);

  // Couche de sortie
  vector<Neurone> coucheSortie=getCoucheSortie();
  vector<double> sortiesReseauSorties=sortiesReseau[getIndiceCoucheSortie()];

  if(!isSoftmax)
  {
    for(size_t i=0; i<coucheSortie.size() && couches.size()>1; i++) // Pour chacun des neurones de sortie
      sortiesReseauSorties[i]= coucheSortie[i].Evaluer(sortiesReseau[getIndiceCoucheSortie()-1], epsilon);
  }
  else
  {
    // Calculer la somme des exponentielles des différents potentiels des neurones
    vector<double> potentiels;
    CalculerPotentielCoucheSortie(sortiesReseau[getIndiceCoucheSortie()-1], potentiels, epsilon);
    double sommeExponentiellePotentiels=CalculerSommeExponentielle(potentiels);

    for(size_t i=0; i<coucheSortie.size(); i++) // Pour chacun des neurones de sortie
      sortiesReseauSorties[i]= Softmax(potentiels[i], sommeExponentiellePotentiels); // Calculer la probabilité de la réponse via Softmax
  }

  return sortiesReseau[getIndiceCoucheSortie()];
}

void ReseauNeurone::CalculerPotentielCoucheSortie(vector<double> &entrees, vector<double> &dst)
{
  vector<Neurone> coucheSortie=getCoucheSortie();

  if(dst.size()<coucheSortie.size())
    dst=vector<double>(coucheSortie.size());

  for(size_t i=0; i<coucheSortie.size(); i++)
    dst[i]=coucheSortie[i].CalculerPotentiel(entrees);
}

void ReseauNeurone::CalculerPotentielCoucheSortie(vector<double> &entrees, vector<double> &dst, double epsilon)
{
  vector<Neurone> coucheSortie=getCoucheSortie();

  if(dst.size()<coucheSortie.size())
    dst=vector<double>(coucheSortie.size());

  for(size_t i=0; i<coucheSortie.size(); i++)
    dst[i]=coucheSortie[i].CalculerPotentiel(entrees, epsilon);
}

size_t ReseauNeurone::getIndiceCoucheSortie()
{
  return couches.size()-1;
}

vector<Neurone> &ReseauNeurone::getCoucheSortie()
{
  return couches[getIndiceCoucheSortie()];
}

string ReseauNeurone::PoidsToString()
{
  ostringstream strs;
  for(vector<Neurone> &couche : couches)
  {
    for(Neurone &n : couche)
      strs << n.PoidsToString() << "\t\t";
    strs << "\t \t \t";

    strs << endl;
  }

  return strs.str();
}

string ReseauNeurone::SortiesToString()
{
  ostringstream strs;
  for(vector<double> &sorties : sortiesReseau)
  {
    for(double &sortie : sorties)
      strs << sortie << '\t';

    strs << endl;
  }

  return strs.str();
}
