﻿#include "donnees.h"

bool ChargerCSV(const string &nomFichier, vector<vector<double>> &dstX, vector<vector<double>> & dstY, size_t quantite, vector<double> &moyennes, vector<double> &ecarttypes)
{
    ifstream fichier(nomFichier);
    int num, row, col, tailleY, tailleX;

    // Lire en-tête
    if(!fichier.is_open() || !LireEnteteCSV(&fichier, &num, &row, &col, &tailleY, &tailleX))
        return false;

    if(quantite==0)
        quantite=num;

    vector<double> xTmp(tailleX), yTmp(tailleY), tmp(tailleX+tailleY);
    dstX = vector<vector<double>>(num);
    dstY = vector<vector<double>>(num);
    // Lire les data
    for(size_t h=0; h<(size_t) quantite && h<(size_t) num; h++)
    {
        try
        {
            if(!LireLigneCSV(tmp, fichier))
                return false;
        }
        catch (...)
        {
            /* Lire Ligne CSV peut lancer l'exception invalid_argument à cause de stod (string to double conversion)
       * Exemple si il y a "abc" au lieu de "12.34" -> pas de conversion possible */
            //cerr << "Invalid argument: " << ia.what() << endl; // DEBUG : affiche le nom de la classe de l'exception
            std::exception_ptr p = std::current_exception();
            return false;
        }

        std::copy(tmp.begin(), tmp.begin()+tailleX, xTmp.begin());
        std::copy(tmp.begin()+tailleX, tmp.begin()+tailleX+tailleY, yTmp.begin());

        // Insérer dans le vecteur dst
        dstX[h] = xTmp;
        dstY[h] = yTmp;
    }

    // Lire la moyenne de chaque variable d'entrée
    if(moyennes.size()!=(size_t)tailleX) // Si le vecteur a déjà été alloué et qu'il n'a pas la bonne taille
        moyennes = vector<double>(tailleX);

    if(!LireLigneCSV(moyennes, fichier, SEP))
        goto fermer_fichier;

    // Lire l'écart-type de chaque variable d'entrée
    if(ecarttypes.size()!=(size_t)tailleX) // Si le vecteur a déjà été alloué et qu'il n'a pas la bonne taille
        ecarttypes = vector<double>(tailleX);

    if(!LireLigneCSV(ecarttypes, fichier, SEP))
        goto fermer_fichier;

fermer_fichier:
    fichier.close();

    return true;
}

bool ChargerCSV(const string &nomFichier, int qtite, vector<string> &dst)
{
    ifstream fichier(nomFichier);
    int num, row, col, tailleY, tailleX;

    // Lire en-tête
    if(!fichier.is_open() || !LireEnteteCSV(&fichier, &num, &row, &col, &tailleY, &tailleX))
        return false;

    if(qtite==0)
        qtite=num;

    vector<string> labels(num);
    string tmp;

    // Lire les data
    for(size_t h=0; h<(size_t) qtite && h<(size_t) num; h++)
    {
        if(!LireLigneCSV(tmp, fichier))
            return false;

        tmp.erase(std::remove(tmp.begin(), tmp.end(), SEP), tmp.end());
        dst[h]=tmp;
    }

    fichier.close();

    return true;
}


bool EnregistrerCSV(const string &nomFichier, int num, int row, int col, int tailleY, vector<vector<double>> &exemples, vector<vector<double> > &sorties, vector<double> *moyennes, vector<double> *ecarttypes)
{
    ofstream streamDst(nomFichier);

    if(!streamDst.is_open() || !EcrireEnteteCSV(&streamDst, num, row, col, tailleY))
        return false;

    for(size_t i=0; i<(size_t) num; i++)
        EcrireLigneCSV(exemples[i], sorties[i], &streamDst);

    if(moyennes!=NULL)
        EcrireLigneCSV(*moyennes, &streamDst);

    if(ecarttypes!=NULL)
        EcrireLigneCSV(*ecarttypes, &streamDst);

    streamDst.close();

    return true;
}

bool EcrireEnteteCSV(ofstream *stream, int quantite, int formatX1, int formatX2, int formatY)
{
    if(!stream->is_open())
        return false;

    *stream << MAGIC_NUMBER_DATA << ',' << quantite << ',' << formatX1 << ',' << formatX2 << ',' << formatY << endl; // Ecrire en-tête
    return true;
}

bool LireEnteteCSV(ifstream *stream, int *quantite, int *formatX1, int *formatX2, int *formatY, int *formatX)
{
    string dummy;

    if(!getline(*stream, dummy))
        return false;

    vector<string> valeurs = split(dummy, SEP);

    int magic=stoi(valeurs[0]);

    if(magic != MAGIC_NUMBER_DATA && magic != MAGIC_NUMBER_LABEL)
        return false;

    *quantite = stoi(valeurs[1]); *formatX1 = stoi(valeurs[2]);
    *formatX2 = stoi(valeurs[3]); *formatY = stoi(valeurs[4]);

    if(formatX!=NULL)
        *formatX = *formatX1 * *formatX2;

    return true;
}

bool LireEnteteCSV(const string &nomFichier, int *quantite, int *formatX1, int *formatX2, int *formatY, int *formatX)
{
    // Lire fichier
    ifstream fichier(nomFichier);
    bool ok=LireEnteteCSV(&fichier, quantite, formatX1, formatX2, formatY, formatX);
    fichier.close();

    return ok;
}

void EcrireLigneCSV(vector<double> &src, ofstream *stream)
{
    for(size_t i=0; i<src.size()-1; i++)
        *stream << src[i] << SEP;

    if(src.size()>0)
        *stream << src[src.size()-1] << endl;

    stream->flush();
}

void EcrireLigneCSV(vector<double> &entrees, vector<double> &sorties, ofstream *stream)
{
    for(size_t i=0; i<entrees.size(); i++)
        *stream << entrees[i] << SEP;

    for(size_t i=0; i<sorties.size()-1; i++)
        *stream << sorties[i] << SEP;

    *stream << sorties[sorties.size()-1] << endl;
    stream->flush();
}

bool LireEntreeCSV(const string &nomFichier, size_t indexe, vector<double> &entrees, vector<double> &sorties)
{
    string dummy;

    ifstream fichier(nomFichier);
    int num, row, col, tailleY, tailleX;

    LireEnteteCSV(&fichier, &num, &row, &col, &tailleY, &tailleX);

    if(indexe>(size_t) num)
        return false;

    for(size_t h=0; h<(size_t) indexe && h<(size_t) num; h++)
    {
        if(!getline(fichier, dummy)) // Fin du fichier atteint
            return false;
    }

    vector<double> tmp(tailleX+tailleY);
    if(!LireLigneCSV(tmp, fichier))
        return false;

    std::copy(tmp.begin(), tmp.begin()+tailleX, entrees.begin()); // Copier entrées
    std::copy(tmp.begin()+tailleX, tmp.begin()+tailleX+tailleY, sorties.begin()); // Copier sorties

    fichier.close();

    return true;
}

void LireMoyennesEcarttypes(const string &nomFichier, vector<double> &moyennes, vector<double> &ecarttypes)
{
    string dummy;

    ifstream fichier(nomFichier);
    int num, row, col, tailleY, tailleX;

    LireEnteteCSV(&fichier, &num, &row, &col, &tailleY, &tailleX);

    for(size_t h=0; h<(size_t) num && h<(size_t) num; h++)
    {
        if(!getline(fichier, dummy)) // Fin du fichier atteint
            return;
    }

    if(moyennes.size()<(size_t) tailleX)
        moyennes = vector<double>(tailleX);

    vector<double> tmp(tailleX);
    if(!LireLigneCSV(tmp, fichier))
        return;

    std::copy(tmp.begin(), tmp.begin()+tailleX, moyennes.begin());

    if(ecarttypes.size()<(size_t) tailleX)
        ecarttypes = vector<double>(tailleX);

    if(!LireLigneCSV(tmp, fichier))
        return;

    std::copy(tmp.begin(), tmp.begin()+tailleX, ecarttypes.begin());

    fichier.close();
}

bool LireLigneCSV(string &dst, ifstream &src)
{
    if(!src.is_open() || !getline(src, dst))
        return false;

    return true;
}

bool LireLigneCSV(vector<double> &dst, ifstream &src, char sep, int ignoredElements)
{
    if(ignoredElements==-1)
        ignoredElements=0;

    string tmp;
    vector<string> valeurs(dst.size() + ignoredElements);

    if(!src.is_open() || !getline(src, tmp))
        return false;

    split(tmp, sep, valeurs);

    try
    {
        for(size_t i=0; i<dst.size(); i++)
            dst[i] = stod(valeurs[i]);
    }
    catch (/*const std::exception& ia*/...)
    {
        /* stod (string to double conversion) peut lancer l'exception invalid_argument
     * Exemple si valeurs[i] contient "abc" au lieu de "12.34"
     * -> pas de conversion possible */
        //cerr << "Invalid argument: " << ia.what() << endl;
        //std::exception_ptr p = std::current_exception();
        //std::clog <<(p ? p.__cxa_exception_type()->name() : "null") << std::endl;
        return false;
    }

    return true;
}
