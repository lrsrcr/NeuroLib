﻿#include "serialisation.h"

bool EnregistrerNeurones(vector<Neurone> &neurones, string fichierDst)
{
  if(neurones.size()==0) // si pas de neurones
    return false;

  ofstream streamDst(fichierDst);
  //Ecrire le header. +3 (QTITE_META_INFO) car fonction d'activation et dérivée en début de ligne et biais en fin de ligne
  streamDst << MAGIC_NUMBER_NEURONES << SEP << neurones.size() << SEP << neurones[0].poids.size()+QTITE_META_INFO << endl;

  for(Neurone &n : neurones)
    EcrireNeurone(n, streamDst);

  streamDst.close();

  return true;
}

bool ChargerNeurones(vector<Neurone> &neurones, string fichierSrc)
{
  if(!FichierExiste(fichierSrc))
    return false;

  ifstream streamSrc(fichierSrc);
  string strTmp;

  // Lire l'en-tête
  streamSrc >> strTmp;
  vector<string> valeurs = split(strTmp, SEP);

  if(valeurs.size()<3)
    return false;

  int magic=stoi(valeurs[0]), nbNeurones=stoi(valeurs[1]), nbPoids=stoi(valeurs[2]);
  if(magic!=MAGIC_NUMBER_NEURONES)
    return false;

  // Lire les neurones
  neurones=vector<Neurone>(nbNeurones);
  valeurs=vector<string>(nbPoids);

  int i=0;
  while(streamSrc >> strTmp && i<nbNeurones)
  {
    split(strTmp, SEP, valeurs);

    ptrfa_t fa = StringToFonction(valeurs[0]), // récupérer la fonction d'activation
	dfa = StringToFonction(valeurs[1]);

    vector<double> poidsTmp(nbPoids-QTITE_META_INFO); // poids
    for(size_t j=0; j<poidsTmp.size(); j++)
      poidsTmp[j] = stod(valeurs[j+INDICE_DEBUT_POIDS]); // j+2: Sauter le ptr de fct d'activation + derivee

    Neurone n(poidsTmp, fa, dfa);
    n.biais = stod(valeurs[valeurs.size()-1]);
    neurones[i++]=n;
  }

  streamSrc.close();
  return true;
}

bool EnregistrerReseauNeurones(ReseauNeurone &rdn, string fichierDst)
{
  if(rdn.couches.size()==0) // Si RDN vide = pas créé
    return false;

  ofstream streamDst(fichierDst);
  // Ecrire le header
  streamDst << MAGIC_NUMBER_MLP << SEP << rdn.couches.size() << SEP << rdn.couches[0].size() << SEP
	<< rdn.couches[0][0].poids.size()+QTITE_META_INFO << SEP << rdn.getCoucheSortie().size()+QTITE_META_INFO
	<< SEP << (int) rdn.isSoftmax << SEP  << endl;

  for(size_t i=0; i<rdn.couches.size(); i++)
    for(size_t j=0; j<rdn.couches[i].size(); j++)
      EcrireNeurone(rdn.couches[i][j], streamDst);

  return true;
}

bool ChargerReseauNeurones(ReseauNeurone &rdn, string fichierSrc)
{
  if(!FichierExiste(fichierSrc))
    return false;

  ifstream streamSrc(fichierSrc);
  string strTmp;

  // Lire l'en-tête
  if(!getline(streamSrc, strTmp))
  {
    streamSrc.close();
    return false;
  }
  vector<string> valeurs = split(strTmp, SEP);

  if(valeurs.size()<6)
    return false;

  size_t magic=stoi(valeurs[0]), nbCouches=stoi(valeurs[1]), NbNeuronesCaches=stoi(valeurs[2]),
      nbPoids=stoi(valeurs[3]), NbNeuronesSortie=stoi(valeurs[4]);
  bool isSoftmax = stoi(valeurs[5]);

  if(magic!=MAGIC_NUMBER_MLP)
    return false;

  rdn=ReseauNeurone(nbPoids-QTITE_META_INFO, Logistique, LogistiqueDerivee, NbNeuronesCaches, nbCouches-1, NbNeuronesSortie-QTITE_META_INFO);
  rdn.isSoftmax=isSoftmax;

  valeurs=vector<string>(nbPoids);
  size_t i=0;
  while(i<nbCouches-1) // Faire couches cachées
  {
    for(size_t j=0; j<NbNeuronesCaches; j++)
    {
      streamSrc >> strTmp;
      if(strTmp.length()==0)
      {
	i=nbCouches-1;
	break;
      }
      split(strTmp, SEP, valeurs);
      ptrfa_t fa = StringToFonction(valeurs[0]), // récupérer la fonction d'activation & sa dérivée
	 dfa = StringToFonction(valeurs[1]);

      vector<double> poidsTmp(nbPoids-QTITE_META_INFO); // poids
      for(size_t j=0; j<poidsTmp.size(); j++)
	poidsTmp[j] = stod(valeurs[j+INDICE_DEBUT_POIDS]); // j+2: Sauter le ptr de fct d'activation + derivee

      Neurone n(poidsTmp, fa, dfa);
      n.biais = stod(valeurs[valeurs.size()-1]);
      rdn.couches[i][j]=n;
    }
    i++;
  }

  valeurs=vector<string>(NbNeuronesCaches+QTITE_META_INFO);
  vector<double> poidsTmp(NbNeuronesCaches+QTITE_META_INFO); // poids

  // Faire couche de sortie séparément
  for(size_t j=0; j<NbNeuronesSortie-QTITE_META_INFO; j++)
  {
    streamSrc >> strTmp;
    split(strTmp, SEP, valeurs);
    ptrfa_t fa = StringToFonction(valeurs[0]), // récupérer la fonction d'activation
       dfa = StringToFonction(valeurs[1]);
    // Le nombre d'entrées/poids pour un Neurone de sortie est le nombre de neurones cachés dans une couche cachée
    for(size_t k=0; k<NbNeuronesCaches; k++)
      poidsTmp[k] = stod(valeurs[k+INDICE_DEBUT_POIDS]); // j+2: Sauter le ptr de fct d'activation + derivee

    rdn.getCoucheSortie()[j]=Neurone(poidsTmp, fa, dfa);
    rdn.getCoucheSortie()[j].biais = stod(valeurs[valeurs.size()-1]);
  }

  streamSrc.close();
  return true;
}

void EcrireNeurone(Neurone &n, ofstream &flux)
{
  flux << FonctionToString(n.fonctionActivation) << SEP // Fonction d'activation au début
      << FonctionToString(n.fonctionActivationDerivee) << SEP;
  for(double p : n.poids)
    flux << p << SEP;

  flux << n.biais << endl; // Biais à la fin
  flux.flush();
}

vector<string> split(string const& original, char separator)
{
    vector<string> results;
    string::const_iterator start = original.begin();
    string::const_iterator end = original.end();
    string::const_iterator next = original.begin();

    size_t i=0;

    while ( next != end )
    {
	next = find(start, end, separator);

	string valeur = string(start, next);
	if(valeur.length()>0)
	  results.push_back(valeur);

	start = next+1;
	i++;
    }

    return results;
}

void split(string const& original, char separator, vector<string> &dst)
{
    string::const_iterator start = original.begin();
    string::const_iterator end = original.end();
    string::const_iterator next = original.begin();

    size_t i=0;
    while (next != end && i<dst.size())
    {
      next = find(start, end, separator);
      dst[i] = string(start, next);
      start = next + 1;
      i++;
    }
}
