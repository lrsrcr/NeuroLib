﻿#ifndef SERIALISATION_H
#define SERIALISATION_H
/** @file serialisation.h
 * @brief Contient les accès aux fichiers des <b>perceptrons mono/multi-couche(s)</b> contenus dans les fichiers CSV.
 * @author DI TROIA J. , BONNIN P.
 * @version 1.0
 * @date 01 août 2018
 */

#include <vector>
#include <fstream>

#include "Neurone/reseauneurone.h"

/**
  * @brief Le Magic Number des fichiers de données.
 */
#define MAGIC_NUMBER_DATA 0
/**
  * @brief Le Magic Number des fichiers contenant des perceptrons mono-couche.
 */
#define MAGIC_NUMBER_NEURONES 1
/**
  * @brief Le Magic Number des fichiers contenant des perceptrons multi-couche(s).
 */
#define MAGIC_NUMBER_MLP 2
/**
  * @brief Le Magic Number des fichiers contenant que des labels.
 */
#define MAGIC_NUMBER_LABEL 4

/**
  * @brief Utilisé par les fonction de lecture et écriture de perceptrons.
 */
#define INDICE_DEBUT_POIDS 2
/**
  * @brief Utilisé par les fonction de lecture et écriture de perceptrons.
 */
#define QTITE_META_INFO 3

/**
  * @brief Séparateur utilisé dans les fichiers CSV permettant de séparer les différents éléments.
 */
#define SEP ';'

using namespace std;

// CHARGER & SAUVEGARDER DES RDN / NEURONES
/**
 * @brief Enregistre un perceptron mono-couche <b>neurones</b> dans le fichier <b>fichierDst</b>.
 * @param neurones Le perceptron mono-couche à enregistrer.
 * @param fichierDst Le chemin du ficher dans lequel enregistrer le perceptron mono-couche.
 * @return <b>true</b> si tout s'est bien déroulé et <b>false</b> si une erreur est survenue.
 */
bool EnregistrerNeurones(vector<Neurone> &neurones, string fichierDst); // SUPPOSE que tous les neurones ont le même nombre de poids
/**
 * @brief Charge le perceptron mono-couche contenu dans le fichier <b>fichiersSrc</b>.
 * @param neurones Le perceptron mono-couche dans lequel stocker celui contenu dans <b>fichiersSrc</b>.
 * @param fichierSrc Le chemin du ficher contenant le perceptron mono-couche.
 * @return <b>true</b> si tout s'est bien déroulé et <b>false</b> si une erreur est survenue.
 */
bool ChargerNeurones(vector<Neurone> &neurones, string fichierSrc);

/**
 * @brief Enregistre un perceptron multi-couches <b>rdn</b> dans le fichier <b>fichierDst</b>.
 * @param rdn  * Le multi-couches à enregistrer.
 * @param fichierDst Le chemin du ficher dans lequel enregistrer le perceptron multi-couches.
 * @return <b>true</b> si tout s'est bien déroulé et <b>false</b> si une erreur est survenue.
 */
bool EnregistrerReseauNeurones(ReseauNeurone &rdn, string fichierDst);
/**
 * @brief Charge le perceptron multi-couches contenu dans le fichier <b>fichiersSrc</b>.
 * @param rdn Le perceptron multi-couches dans lequel stocker celui contenu dans <b>fichiersSrc</b>.
 * @param fichierSrc Le chemin du ficher contenant le perceptron mono-couche.
 * @return <b>true</b> si tout s'est bien déroulé et <b>false</b> si une erreur est survenue.
 */
bool ChargerReseauNeurones(ReseauNeurone &rdn, string fichierSrc);

/**
 * @brief Ecrit un perceptron mono-couche dans le flux <b>flux</b>.
 * @param n Le perceptron mono-couche à écrire.
 * @param flux Le flux dans lequel écrire le perceptron mono-couche.
 */
void EcrireNeurone(Neurone &n, ofstream &flux);

/**
 * @brief Vérifie que le fichier <b>name</b> existe.
 * @param name Chemin du fichier dont l'existence est vérifiée.
 * @return <b>true</b> si le fichier existe, <b>false</b> sinon.
 */
inline bool FichierExiste(const string& name)
{
  std::ifstream infile(name);
  return infile.good();
}

/**
 * @brief Récupère toutes les chaînes de caractères séparées par <b>separator</b> dans le stirng <b>original</b>.
 *
 * Cette version est plus lente car le nombre d'occurence de <b>separator</b> dans <b>original</b> n'est pas connue.
 * Dès lors, le vecteur est agrandi à chaque occurence de <b>separator</b>.
 *
 * @param original La chaîne de caractères à analyser.
 * @param separator Le séparateur dont chaque occurence provoque la coupure de la chaîne.
 * @return Un vecteur contenant toutes les chaînes de caractères séparées par <b>separator</b> dans le stirng <b>original</b>.
 */
vector<string> split(string const& original, char separator);
/**
 * @brief Récupère toutes les chaînes de caractères séparées par <b>separator</b> dans le stirng <b>original</b> et les stock dans <b>dst</b>.
 *
 * Cette version est optimale car le nombre d'occurence de <b>separator</b> dans <b>original</b> est connue !
 *
 * @param original La chaîne de caractères à analyser.
 * @param separator Le séparateur dont chaque occurence provoque la coupure de la chaîne.
 * @param dst Un vecteur contenant toutes les chaînes de caractères séparées par <b>separator</b> dans le stirng <b>original</b>.
 */
void split(string const& original, char separator, vector<string> &dst); // Version avec le vecteur déjà alloué

#endif // SERIALISATION_H
