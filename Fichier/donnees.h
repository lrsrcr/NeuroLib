﻿#ifndef DONNEES_H
#define DONNEES_H

/** @file donnees.h
 * @brief Contient les accès aux fichiers de données CSV.
 * @author DI TROIA J. , BONNIN P.
 * @version 1.0
 * @date 01 août 2018
 */

#include <Fichier/serialisation.h>
#include <Neurone/reseauneurone.h>
#include <Fonctions/fonctionactivation.h>

using namespace std;

/**
 * @brief Charge à partir d'un fichier des données en mémoire.
 *
 * Charge les exemples contenue dans le fichier <b>nomFichier</b> en mémoire dans <b>dstX</b> et les sorties correspondantes dans <b>dstX</b>.
 * Si <b>quantite</b> est égal à 0, alors l'entierté des données sont chargées en mémoires.
 * Si les moyennes et écarts-types des données sont présents à la fin du fichier, ceux-ci seront stocker dans les variables <b>moyennes</b> et <b>ecarttypes</b>.
 *
 * @param nomFichier Le nom du fichier contenant les données.
 * @param dstX Matrice destinées à contenir les données.
 * @param dstY Matrice destinées à contenir les vecteurs de sorties.
 * @param quantite Quantité désirée de données devant être lue.
 * @param moyennes Vecteur contenant les moyennes des différents entrées, si les moyennes sont présentes à la fin du fichier.
 * @param ecarttypes Vecteur contenant les écarts-types des différents entrées, si les écarts-types sont présents à la fin du fichier.
 * @return <b>true</b> si tout s'est bien déroulé et <b>false</b> si une erreur est survenue.
 */
bool ChargerCSV(const string &nomFichier,  vector<vector<double>> &dstX, vector<vector<double> > &dstY, size_t quantite, vector<double> &moyennes, vector<double> &ecarttypes);
/**
 * @brief Charge à partir d'un fichier des chaînes de caractères en mémoire.
 *
 * Une ligne du fichier <b>nomFichier</b> correspond à un enregistrement chargé en mémoire.
 * Si <b>quantite</b> est égal à 0, alors l'entierté des données sont chargées en mémoires.
 *
 * @param nomFichier Le nom du fichier contenant les données.
 * @param qtite Quantité désirée de lignes devant être lue.
 * @param dst Vecteur destiné à contenir les lignes lues dans le fichier.
 * @return <b>true</b> si tout s'est bien déroulé et <b>false</b> si une erreur est survenue.
 */
bool ChargerCSV(const string &nomFichier, int qtite, vector<string> &dst);

/**
 * @brief Crée le fichier <b>nomFichier</b> et enregistre les données contenues dans <b>exemples</b> et <b>sorties</b>.
 *
 * Le fichier créé est un fichier de <b>données</b> dont le header est: 0, <b>num</b>, <b>row</b>, <b>col</b>, <b>tailleY</b>.
 * De plus, si les pointeurs <b>moyennes</b> et <b>ecarttypes</b> ne sont pas null, les moyennes et écarts-types des données sont ajoutées à la fin du fichier.
 *
 * @param nomFichier Le nom du fichier contenant les données.
 * @param num Le nombre d'exemples contenus dans <b>exemples</b>
 * @param row Le nombre de lignes dans les images enregistrées.
 * @param col Le nombre de colonnes dans les images enregistrées.
 * @param tailleY Le nombre de labels possibles existant dans le jeu de données.
 * @param exemples Les exemples à enregistrer, donc chacun fait une taille de row * col.
 * @param sorties Les sorties correspondantes aux exemples, donc chaque ligne correspond à un exemple. La taille de chaque ligne vaut <b>tailleY</b>.
 * @param moyennes Pointeur éventuel vers les moyennes des données.
 * @param ecarttypes Pointeur éventuel vers les écarts-types des données.
 * @return <b>true</b> si tout s'est bien déroulé et <b>false</b> si une erreur est survenue.
 */
bool EnregistrerCSV(const string &nomFichier, int num, int row, int col, int tailleY, vector<vector<double>> &exemples, vector<vector<double>> &sorties, vector<double> *moyennes=NULL, vector<double> *ecarttypes=NULL);

/**
 * @brief Lit l'entête du fichier pointé par <b>stream</b>.
 * @param stream Flux du fichier dans lequel l'en-tête est lue.
 * @param quantite Quantité désirée de données devant être lue.
 * @param formatX1 Pointeur qui pointera vers le nombre de lignes des images contenues dans le fichier.
 * @param formatX2 Pointeur qui pointera vers le nombre de colonnes des images contenues dans le fichier.
 * @param formatY Pointeur qui pointera vers la taille du one hot encoded vector des images contenues dans le fichier.
 * @param formatX Contient le produit de <b>formatX1</b> et de <b>formatX2</b>.
 * @return <b>true</b> si tout s'est bien déroulé et <b>false</b> si une erreur est survenue.
 */
bool LireEnteteCSV(ifstream *stream,  int *quantite, int *formatX1, int *formatX2, int *formatY, int *formatX);
/**
 * @brief Lit l'entête du fichier <b>nomFichier</b>.
 * @param nomFichier Le nom du fichier contenant les données.
 * @param quantite Quantité désirée de données devant être lue.
 * @param formatX1 Pointeur qui pointera vers le nombre de lignes des images contenues dans le fichier.
 * @param formatX2 Pointeur qui pointera vers le nombre de colonnes des images contenues dans le fichier.
 * @param formatX Pointeur qui pointera vers la taille du one hot encoded vector des images contenues dans le fichier.
 * @param formatY Contient le produit de <b>formatX1</b> et de <b>formatX2</b>.
 * @return <b>true</b> si tout s'est bien déroulé et <b>false</b> si une erreur est survenue.
 */
bool LireEnteteCSV(const string &nomFichier, int *quantite, int *formatX1, int *formatX2, int *formatY, int *formatX);

/**
 * @brief Ecrit l'en-tête d'un fichier de données pointé par <b>stream</b>.
 * @param stream Flux du fichier dans lequel l'en-tête sera écrit.
 * @param quantite Quantité désirée de données devant être lue.
 * @param formatX1 Pointeur qui pointera vers le nombre de lignes des images contenues dans le fichier.
 * @param formatX2 Pointeur qui pointera vers le nombre de colonnes des images contenues dans le fichier.
 * @param formatY Pointeur qui pointera vers la taille du one hot encoded vector des images contenues dans le fichier.
 * @return <b>true</b> si tout s'est bien déroulé et <b>false</b> si une erreur est survenue.
 */
bool EcrireEnteteCSV(ofstream *stream, int quantite, int formatX1, int formatX2, int formatY);

/**
 * @brief Lit l'entrée et le one hot encoded vector dans le fichier <b>nomFichier</b> à la position <b>indexe</b>.
 *
 *
 *
 * @param nomFichier Le nom du fichier contenant les données.
 * @param indexe Le numéro de l'entrée à lire.
 * @param entrees Le vecteur contenant les données.
 * @param sorties Le vecteur contenant le one hot encoded vector.
 * @return <b>true</b> si tout s'est bien déroulé et <b>false</b> si une erreur est survenue.
 */
bool LireEntreeCSV(const string &nomFichier, size_t indexe, vector<double> &entrees, vector<double> &sorties);
/**
 * @brief Lit la prochaine ligne disponible dans le flux passé en paramètre.
 * @param dst La chaîne contenant la ligne lue
 * @param src Flux du fichier dans lequel la ligne sera lue.
 * @return <b>true</b> si tout s'est bien déroulé et <b>false</b> si une erreur est survenue.
 */
bool LireLigneCSV(string &dst, ifstream &src);
/**
 * @brief Lit la prochaine ligne disponible dans le flux passé en paramètre et stock le résultat dans <b>dst</b>.
 *
 * Fonction inutile.
 *
 * @param dst Le vecteur contenant les données lues.
 * @param src Flux du fichier dans lequel l'exemple sera lu.
 * @param sep Le séparateur sur lequel splitter la chaîne de caractère lue afin d'en extraire les données.
 * @param ignoredElements
 * @return <b>true</b> si tout s'est bien déroulé et <b>false</b> si une erreur est survenue.
 */
bool LireLigneCSV(vector<double> &dst, ifstream &src, char sep=SEP, int ignoredElements=-1);

/**
 * @brief Ecrit une ligne de données d'un fichier pointé par <b>stream</b>.
 * @param src Ligne de données à écrire dans le fichier.
 * @param stream Flux du fichier dans lequel la ligne sera écrite.
 */
void EcrireLigneCSV(vector<double> &src, ofstream *stream);
/**
 * @brief Ecrit une ligne de données avec le one hot encoded vector d'un fichier de données pointé par <b>stream</b>.
 * @param entrees Les données à écrire.
 * @param sorties Le one hot encoded vector correspondant aux données.
 * @param stream Le flux de données sur lequel écrire.
 */
void EcrireLigneCSV(vector<double> &entrees, vector<double> &sorties, ofstream *stream);

/**
 * @brief Lis les moyennes et les écarts-types éventuellements présents à la fin d'un fichier de données.
 * @param nomFichier Le nom du fichier contenant les données.
 * @param moyennes Vecteur acceuillant les moyennes lues dans le fichier <b>nomFichier</b>.
 * @param ecarttypes Vecteur acceuillant les écarts-types lue dans le fichier <b>nomFichier</b>.
 */
void LireMoyennesEcarttypes(const string &nomFichier, vector<double> &moyennes, vector<double> &ecarttypes);

/**
 * @brief Lire l'en-tête d'un fichier <i>image</i> de la base de donnée MNIST (IDX3-UBYTE).
 * @param fichier Le pointeur du fichier contenant les données.
 * @param nombre Contient la quantité d'images dans le fichier IDX3-UBYTE après lecture.
 * @param lignes Contient la dimension verticale (hauteur) des images du fichier après lecture.
 * @param colonnes Contient la dimension horizontale(largeur) des images du fichier après lecture.
 */
void LireImagesLignesColonnes(FILE *fichier, unsigned int &nombre, unsigned int &lignes, unsigned int &colonnes);

#endif // DONNEES_H
