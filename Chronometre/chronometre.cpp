#include "chronometre.h"

Chronometre::Chronometre() {
  Reset();
}

void Chronometre::Start() {
  Reset();
  t1=high_resolution_clock::now();
}

string Chronometre::Stop() {
  t2=high_resolution_clock::now();

  CalculerTemps(t1, t2);
  return Formatter();
}

string Chronometre::Formatter(nanoseconds ns) {
 CalculerTemps(t_time(nanoseconds::zero()), t_time(ns));
 return Formatter();
}

string Chronometre::Formatter()
{
  std::ostringstream out;

  if(h>hours::zero())
    out << h.count() << "h ";

  if(min>minutes::zero())
    out << min.count() << "m ";

  if(sec>seconds::zero())
    out << sec.count() << "s ";

  if(ms>milliseconds::zero())
	out << ms.count() << "ms ";

  if(us>microseconds::zero())
	out << us.count() << "us ";

  if(ns>nanoseconds::zero())
	out << ns.count() << "ns";
  else if(ns==nanoseconds::zero() && out.str().size()==0) {
	out << ns.count() << "ns";
  }

  return out.str();
}

void Chronometre::Reset() {
  counter=0;
  mean=0;
  ns=nanoseconds::zero();
  h=hours::zero();
  min=minutes::zero();
  sec=seconds::zero();
  ms=milliseconds::zero();
  us=microseconds::zero();
}

void Chronometre::CalculerTemps(t_time time1, t_time time2) {
  ns = duration_cast<nanoseconds>(time2-time1);

  h = duration_cast<hours>(ns);
  ns -= h;

  min = duration_cast<minutes>(ns);
  ns -= min;

  sec = duration_cast<seconds>(ns);
  ns -= sec;

  ms = duration_cast<milliseconds>(ns);
  ns -= ms;

  us = duration_cast<microseconds>(ns);
  ns -= us;
}


