#ifndef CHRONOMETRE_H
#define CHRONOMETRE_H

/** @file chronometre.h
 * @brief Contient une classe Chronometre.
 * @author DI TROIA J. , BONNIN P.
 * @version 1.0
 * @date 01 août 2018
 */

#include <chrono>
#include <iomanip>
#include <string>
#include <sstream>

using namespace std;
using namespace chrono;

/**
 * @brief t_time Type contenant un <b>time_point</b> avec la meilleure résolution disponible sur le système courant
 * ainsi qu'une précision de type <b>double</b> allant jusqu'à la nanoseconde.
 */
typedef time_point<high_resolution_clock, duration<double, std::nano>> t_time;

/**
 * @class Chronometre
 * @brief Classe représentant un chronomètre effectuant des mesure de temps et fournissant une chaîne de caractères destinée à l'affichage.
 */
class Chronometre
{
private:
  t_time t1,t2;         /*< Les 2 temps enregistré: début et fin. */
  double mean; //          /*< Mesure de la moyenne si exécution à répétition */
  int counter; //        /*< Compte le nombre d'exécution si répétition */

  hours h;             /*< Contient les heures */
  minutes min;         /*< Contient les minutes */
  seconds sec;         /*< Contient les secondes */
  milliseconds ms;     /*< Contient les millisecondes */
  microseconds us;     /*< Contient les microsecondes */ // for µs
  nanoseconds ns;      /*< Contient les nanosecondes */

public:
  /**
   * @brief Constructeur par défaut. Initialise les variables à 0.
   */
  Chronometre();

  /**
   * @brief Démarre le chronomètre.
   *
   * Ré-initialise l'ensemble des variables temporelle, tous les résultats obtenus sont alors perdus.
   */
  void Start();

  /**
   * @brief Arrête le chronomètre.
   * @return Une chaîne de caractères contenant le temps mesuré par le chronomètre.
   *
   * La chaîne retournée ne contient que les informations utiles, c'est-à-dire les variables temporelles non-nulles.
   */
  string Stop();

  /**
   * @brief Calcule le nombre d'heures, de minutes, de secondes, de millisecondes, de microsecondes et de nanosecondes contenus dans le paramètre <b>ns</b>.
   *
   * La chaîne retournée ne fournit que les informations utiles.
   * Par exemple, s'il y a 0 minute et 3 secondes, le string ne contiendra que 3 secondes.
   *
   * @param ns Le nombre de nanosecondes à convertir.
   * @return Une chaîne de caractères contenant les résultats du chronométrage.
   */
  string Formatter(nanoseconds ns);

  /**
   * @brief Calcule le nombre d'heures, de minutes, de secondes, de millisecondes, de microsecondes et de nanosecondes contenus dans la variable membre <b>ns</b>.
   *
   * La chaîne retournée ne fournit que les informations utiles.
   * Par exemple, s'il y a 0 minute et 3 secondes, le string ne contiendra que 3 secondes.
   *
   * @return Une chaîne de caractères contenant les résultats du chronométrage.
   */
  string Formatter();
  /**
   * @brief Calcule la différence de temps entre deux moments de début et de fin.
   *
   * Le résultat est stocké dans les différentes variables temporelles.
   *
   * @param time1 Moment de début.
   * @param time2 Moment de fin.
   */
  void CalculerTemps(t_time time1, t_time time2);

  /**
   * @brief Ré-initialise toutes les variables temporelles.
   */
  void Reset();
};

#endif // CHRONOMETRE_H
