#-------------------------------------------------
#
# Project created by QtCreator 2018-02-15T17:11:57
#
#-------------------------------------------------

QT       -= core gui

TARGET = NeuroLib
TEMPLATE = lib

DEFINES += NEUROLIB_LIBRARY
CONFIG += c++14

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    Neurone/neurone.cpp \
    Fonctions/fonctionactivation.cpp \
    Apprentissage/apprentissage.cpp \
    Fonctions/fonctionerreur.cpp \
    Neurone/reseauneurone.cpp \
	Fichier/serialisation.cpp \
    Fichier/donnees.cpp \
		Chronometre/chronometre.cpp

HEADERS += \
        neurolib_global.h \ 
    Neurone/neurone.h \
    Fonctions/fonctionactivation.h \
    Apprentissage/apprentissage.h \
    Fonctions/fonctionerreur.h \
    Neurone/reseauneurone.h \
	Fichier/serialisation.h \
    Fichier/donnees.h \
		Chronometre/chronometre.h

unix {
    target.path = /usr/lib
    INSTALLS += target
}

DISTFILES +=
