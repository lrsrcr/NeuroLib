﻿#include "fonctionactivation.h"

double Logistique(double x)
{
  //f(x) = 1 / ( 1 + exp(-x) )
  return 1/(1+exp(-x));
}

double LogistiqueDerivee(double x)
{
  //f'(x) = f(x) x ( 1 - f(x) )
  double fx = Logistique(x);
  return fx*(1-fx);
}

double TangenteHyperbolique(double x)
{
  //f(x) = tanh(x)
  return tanh(x);
}

double TangenteHyperboliqueDerivee(double x)
{
  //f(x) = 1-tanh(x)^2
  return 1 - pow(tanh(x), 2);
}

double Lineaire(double x)
{
  //f(x) = x
  return x;
}

double LineaireDerivee(double x)
{
  //f'(x) = (x)' = 1
  return 1;
}

double Heaviside(double x)
{
  //f(x) = 0 si x<theta (seuil) = 0, sinon 1
  if(x<0)
    return 0;
  else
    return 1;
}

double Signe(double x)
{
  if(x>0.0f)
    return 1;
  else
    return -1;
}

double ReLU(double x)
{
  //f(x) = max(0,x)
  return std::max<double>(0, x);
}

double ReLuDerivee(double x)
{
  //f(x) = 0 si x<0, sinon 1
  if(x<0)
    return 0;
  else
    return 1;
}

double Softmax(double potentiel, double sommeExpPotentiels)
{
  return exp(potentiel)/sommeExpPotentiels;
}

double LogSoftmax(double potentiel, double sommeExpPotentiels)
{
  return - potentiel + log(sommeExpPotentiels);
}

double SoftmaxDerivee(vector<double> &potentiels, size_t i)
{
  // Dérivée Softmax
  double exp1 = exp(potentiels[i]),
    somme = CalculerSommeExponentielle(potentiels),
    numerateur = exp1*somme,
    denominateur = pow(somme + exp1, 2),
    dSi= numerateur/denominateur;

  /* Dérivée LogSoftmax : Source https://ljvmiranda921.github.io/notebook/2017/08/13/softmax-and-the-negative-log-likelihood/#negative-log-likelihood
   * Ne fonctionne pas .... ?
   */
  /*double sommePotentiels = CalculerSommeExponentielle(potentiels);
  double dSi = Softmax(potentiels[i], sommePotentiels) -1 ;*/

  // Dérivée LogSoftmax : source: https://math.stackexchange.com/questions/2013050/log-of-softmax-function-derivative
  /*double sommePotentiels = CalculerSommeExponentielle(potentiels),
    right = 0.0f;

  for(size_t i=0; i<potentiels.size(); i++)
      right += Softmax(potentiels[i], sommePotentiels) * potentiels[i];

  double dSi = potentiels[i] - right;*/

  return dSi;
}

double CalculerSommeExponentielle(vector<double> &x)
{
  double acc=0.0f;
  for(size_t i=0; i<x.size(); i++)
    acc+= exp(x[i]);

  return acc;
}

string FonctionToString(ptrfa_t fa)
{
  if(fa == Logistique)
    return "LOGISTIQUE";
  else if(fa == LogistiqueDerivee)
    return "LOGISTIQUEDERIVEE";
  else if(fa == TangenteHyperbolique)
    return "TANGENTEHYPERBOLIQUE";
  else if(fa == TangenteHyperboliqueDerivee)
    return "TANGENTEHYPERBOLIQUEDERIVEE";
  else if(fa == Lineaire)
    return "LINEAIRE";
  else if(fa == LineaireDerivee)
    return "LINEAIREDERIVEE";
  else if(fa == Heaviside)
    return "HEAVISIDE";
  else if(fa == Signe)
    return "SIGNE";
  else if(fa == ReLU)
    return "RELU";
  else if(fa == ReLuDerivee)
    return "RELUDERIVEE";
  else
    return "UNKOWN";
}

ptrfa_t StringToFonction(string fonctionName)
{
  // Transformer les caractères du paramètre en majuscule.
  transform(fonctionName.begin(), fonctionName.end(), fonctionName.begin(), ::toupper);

  if(fonctionName == "LOGISTIQUE")
    return Logistique;
  else if(fonctionName == "LOGISTIQUEDERIVEE")
    return LogistiqueDerivee;
  else if(fonctionName == "TANGENTEHYPERBOLIQUE")
    return TangenteHyperbolique;
  else if(fonctionName == "TANGENTEHYPERBOLIQUEDERIVEE")
    return TangenteHyperboliqueDerivee;
  else if(fonctionName == "LINEAIRE")
    return Lineaire;
  else if(fonctionName == "LINEAIREDERIVEE")
    return LineaireDerivee;
  else if(fonctionName == "HEAVISIDE")
    return Heaviside;
  else if(fonctionName == "SIGNE")
    return Signe;
  else if(fonctionName == "RELU")
    return ReLU;
  else if(fonctionName == "RELUDERIVEE")
    return ReLuDerivee;
  else
    return NULL;
}
