﻿#ifndef FONCTIONACTIVATIONC_H
#define FONCTIONACTIVATIONC_H

/** @file fonctionactivation.h
 * @brief Contient les fonctions d'activation et leur dérivées.
 * @author DI TROIA J. , BONNIN P.
 * @version 1.0
 * @date 01 août 2018
 */

#include <cmath>
#include <vector>
#include <algorithm>

using namespace std;

// Pointeur fonction d'activation et dérivée fonction activation
typedef double (*ptrfa_t)(double); /*!< Pointeur fonction d'activation et dérivée fonction activation */

// Fonctions continues
/**
 * @brief Applique la fonction Logistique \f$\sigma = \frac{1}{1+e^{-x}} \f$.
 * @param x Valeur de la variable de la fonction Logistique.
 * @return  La sortie de la fonction Logistique avec comme variable <b>x</b>.
 */
double Logistique(double x);
/**
 * @brief Applique la fonction Logistique Derivée \f$\sigma^{\prime} = \sigma(x) (1-\sigma(x)) \f$.
 * @param x Valeur de la variable de la fonction Logistique Derivée.
 * @return La sortie de la fonction Logistique Derivée avec comme variable <b>x</b>.
 */
double LogistiqueDerivee(double x);

/**
 * @brief Applique la fonction Tangente Hyperbolique \f$ tanh(x) \f$.
 * @param x Valeur de la variable de la fonction Tangente Hyperbolique.
 * @return La sortie de la fonction  Tangente Hyperbolique avec comme variable <b>x</b>.
 */
double TangenteHyperbolique(double x);
/**
 * @brief Applique la fonction Tangente Hyperbolique Dérivée \f$ 1 - tanh^2(x) \f$.
 * @param x Valeur de la variable de la fonction Tangente Hyperbolique Dérivée.
 * @return La sortie de la fonction  Tangente Hyperbolique Dérivée avec comme variable <b>x</b>.
 */
double TangenteHyperboliqueDerivee(double x);

/**
 * @brief Applique la fonction Linéaire \f$ f(x) = x \f$.
 * @param x Valeur de la variable de la fonction Linéaire.
 * @return La sortie de la fonction  Linéaire avec comme variable <b>x</b>.
 */
double Lineaire(double x);
/**
 * @brief Applique la fonction Linéaire Dérivée \f$ f(x) = 1 \f$
 *
 * Peu importe l'argument <b>x</b>, cette fonction retourne 1.
 *
 * @param x Valeur de la variable de la fonction Linéaire Dérivée, inutile puisque la fonction renvoie toujours 1.
 * @return La sortie de la fonction Linéaire Dérivée, c'est-à-dire <b>1</b>.
 */
double LineaireDerivee(double x);

// Fonctions à seuil
/**
 * @brief Applique la fonction d'Heaviside \f$ f(x) =  x\f$ si \f$x > 0\f$, \f$0\f$ sinon.
 * @param x Valeur de la variable de la fonction Heaviside.
 * @return La sortie de la fonction  Heaviside avec comme variable <b>x</b>.
 */
double Heaviside(double x);
/**
 * @brief Applique la fonction Signe \f$ f(x) =  1\f$ si \f$x > 0\f$, \f$-1\f$ sinon.
 * @param x Valeur de la variable de la fonction Signe.
 * @return La sortie de la fonction Signe avec comme variable <b>x</b>.
 */
double Signe(double x);

/**
 * @brief Applique la fonction Rectified Linear Unit (ReLU) \f$ f(x) =  x\f$ si \f$x > 0\f$, \f$0\f$ sinon.
 * @param x Valeur de la variable de la fonction ReLU.
 * @return La sortie de la fonction ReLU avec comme variable <b>x</b>.
 */
double ReLU(double x);
/**
 * @brief Applique la fonction Rectified Linear Unit (ReLU) Dérivée \f$ f(x) =  1\f$ si \f$x > 0\f$, \f$0\f$ sinon.
 * @param x Valeur de la variable de la fonction ReLU Dérivée.
 * @return La sortie de la fonction ReLU Dérivée avec comme variable <b>x</b>.
 */
double ReLuDerivee(double x);

// Fonction Softmax
/**
 * @brief Applique la fonction Softmax \f$ S_i(\mathbf{\nu}) = \frac{e^{\nu_i}}{\sum_j^N e^{\nu_j} } \f$.
 * @param potentiel Potentiel \f$\nu_i\f$ duquel dépend la sortie de la fonction Softmax (numérateur).
 * @param sommeExpPotentiels La somme des exponentielles des potentiels \f$\sum_j^N e^{\nu_j}\f$ de la couche de neurones de sortie (dénominateur).
 * @return La sortie de la fonction Softmax en fonction des paramètres.
 */
double Softmax(double potentiel, double sommeExpPotentiels);
/**
 * @brief Calcule la fonction LogSoftmax \f$ log(S_i(\mathbf{\nu}) = -\nu_i + log(\sum_j^N e^{\nu_j}) \f$.
 * @param potentiel Potentiel \f$\nu_i\f$ duquel dépend la sortie de la fonction Softmax (numérateur).
 * @param sommeExpPotentiels La somme des exponentielles des potentiels \f$\sum_j^N e^{\nu_j}\f$ de la couche de neurones de sortie (dénominateur).
 * @return La sortie de la fonction LogSoftmax en fonction des paramètres.
 */
double LogSoftmax(double potentiel, double sommeExpPotentiels);
/**
 * @brief Calcule la dérivée de la fonction Softmax \f$ S^{\prime}_i (\mathbf{\nu}) = \frac{ e{\nu_i}\sum_j^N e^{\nu_j} }{ \left( \sum_j^N e^{\nu_j} + e{\nu_i} \right)^2 } \f$.
 * @param potentiels Les potentiels de la couche de neurones de sortie.
 * @param i Indexe du neurone considéré pour calculé la fonction Softmax Dérivée.
 * @return La sortie de la fonction Softmax Derivée en fonction des paramètres.
 */
double SoftmaxDerivee(vector<double> &potentiels, size_t i);
/**
 * @brief Calcule la somme des exponentielles du vecteur <b>x</b> passé en paramètre.
 *
 * Utilisée par les fonction Softmax, LogSoftmax, SoftmaxDerivee.
 *
 * @param x Un vecteur contenant les potentiels d'une couche de neurones de sortie.
 * @return La somme des exponentielle selon la formule: \f$ \sum_j^N e^{x_j} \f$.
 */
double CalculerSommeExponentielle(vector<double> &x);


// Manipulation dans des fichiers, flux, ...
/**
 * @brief Permet de récupérer le nom de la fonction d'activation (dérivée ou non) sous forme de chaîne de caractère.
 *
 * Sert notamment dans la sérialisation des perceptrons mono/multi-couche(s) ou encore dans l'affichage graphique.
 * Fonction complémentaire à <b>StringToFonction</b>.
 *
 * @param fa Le pointeur de la fonction d'activation (dérivée ou non) dont on souhaite avoir le nom sous forme de chaîne de caractères.
 * @return Une chaîne de caractères contenant le nom de la fonction d'activation (dérivée ou non) en <b>majuscule</b>.
 */
string FonctionToString(ptrfa_t fa);
/**
 * @brief Permet de récupérer le pointeur de fonction correspondant à celui de la chaîne de caractères passée en paramètre.
 *
 * Sert notamment dans la sérialisation des perceptrons mono/multi-couche(s) ou encore dans l'affichage graphique.
 * Fonction complémentaire à <b>StringToFonction</b>.
 *
 * @param fonctionName Le nom de la fonction d'activation (dérivée ou non) dont on souhaite récupérer le pointeur.
 * @return Un pointeur de la fonction d'activation (dérivée ou non).
 */
ptrfa_t StringToFonction(string fonctionName);

#endif // FONCTIONACTIVATIONC_H
