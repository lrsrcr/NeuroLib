﻿#include "fonctionerreur.h"

double FonctionPerteQuadratique(double sortieReelle, double sortieDesiree)
{
  return 0.5 * pow(sortieDesiree-sortieReelle,2);
}

double FonctionPerteEntropieRelative(double sortieReelle, double sortieDesiree)
{
  double tmp = - (sortieDesiree * log(sortieReelle) + (1-sortieDesiree) * log(1-sortieReelle));
  return tmp;
}

double FonctionPerteEntropieRelativeGenerale(double sortieObtenue, double sortieDesiree)
{
  double tmp = - (sortieDesiree * log(sortieObtenue));
  return tmp;
}


double CorrectionPerteQuadratique(Neurone &n, double sortieDesiree, double &deltasW)
{
  double errtmp = (sortieDesiree-n.y);
  deltasW = errtmp  * n.getDerivee();

  return FonctionPerteQuadratique(n.y, sortieDesiree);
}

double  CorrectionPerteEntropieRelative(Neurone &n, double sortieDesiree, double &deltasW)
{
  deltasW = (sortieDesiree-n.y); // Revient à faire: deltasW = n.getDerivee() * (sortieDesiree-n.y)/(n.y*(1-n.y))
  return FonctionPerteEntropieRelative(n.y, sortieDesiree);
}

double CalculerErreurGlobale(vector<Neurone> &neuronesSortie, vector<double> &sortiesDesirees, bool erreurQuadratique, bool utiliserSoftmax)
{
  double erreurGlobale=0.0f;

    for(size_t i=0; i<neuronesSortie.size(); i++)
    {
      double SortieVoulue=sortiesDesirees[i],
	  sortieObtenue=neuronesSortie[i].y;

      if(erreurQuadratique)
        erreurGlobale += FonctionPerteQuadratique(sortieObtenue, SortieVoulue);
      else
      {
        if(utiliserSoftmax)
          erreurGlobale += FonctionPerteEntropieRelativeGenerale(sortieObtenue, SortieVoulue);
        else
          erreurGlobale += FonctionPerteEntropieRelative(sortieObtenue, SortieVoulue);
      }
    }

  return erreurGlobale;
}

double FonctionDifferenceCentrale(double grad1, double grad2, double epsilon)
{
  return - (grad2-grad1)/(2*epsilon); // "-" car dans la descente de gradient, on RETIRE la pente.
}
