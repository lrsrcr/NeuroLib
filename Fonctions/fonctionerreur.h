﻿#ifndef FONCTIONERREUR_H
#define FONCTIONERREUR_H

/** @file fonctionerreur.h
 * @brief Contient les différentes fonctions d'erreur.
 * @author DI TROIA J. , BONNIN P.
 * @version 1.0
 * @date 01 août 2018
 */

#include <cmath>
#include <limits>
#include <vector>

#include "Neurone/neurone.h"

using namespace std;

/**
 * @brief Calcule l'erreur quadratique entre la sortie souhaitée \f$ t \f$ et la sortie obtenue \f$ o \f$: \f$ E = \frac{1}{2} (t-o)^2 \f$.
 * @param sortieReelle La sortie obtenue \f$ o \f$.
 * @param sortieDesiree La sortie souhaitée \f$ t \f$.
 * @return L'erreur quadratique entre les deux sorties passées en paramètre.
 */
double FonctionPerteQuadratique(double sortieReelle, double sortieDesiree);
/**
 * @brief Calcule l'erreur d'entropie relative entre la sortie souhaitée \f$ t \f$ et la sortie obtenue \f$ o \f$: \f$ E = - (t * log(o) + (1-t) * log(1-o)) \f$.
 *
 * Utilisable si le réseau de neurones ne possède que deux sorties possibles et un seul neurone de sortie.
 *
 * @param sortieReelle La sortie obtenue \f$ o \f$.
 * @param sortieDesiree La sortie souhaitée \f$ t \f$.
 * @return L'erreur d'entropie relative entre les deux sorties passées en paramètre.
 */
double FonctionPerteEntropieRelative(double sortieReelle, double sortieDesiree);

/**
 * @brief Calcule l'erreur d'entropie relative entre la sortie souhaitée \f$ t \f$ et la sortie obtenue \f$ o \f$: \f$ E = - (t * log(o)) \f$.
 * @param sortieObtenue La sortie obtenue \f$ o \f$.
 * @param sortieDesiree La sortie souhaitée \f$ t \f$.
 * @return L'erreur d'entropie relative entre les deux sorties passées en paramètre.
 */
double FonctionPerteEntropieRelativeGenerale(double sortieObtenue, double sortieDesiree);

/**
 * @brief Calcule l'erreur locale d'un neurone de sortie pour une fonction d'erreur quadratique.
 * @param n Référence du neurone de sortie dont on souhaite calculer l'erreur locale.
 * @param sortieDesiree La sortie désirée pour l'exemple considéré.
 * @param deltasW Référence permettant de stocker l'erreur locale calculée.
 * @return La contribution à l'erreur générale du réseau de neurones.
 */
double CorrectionPerteQuadratique(Neurone &n, double sortieDesiree, double &deltasW);
/**
 * @brief Calcule l'erreur locale d'un neurone de sortie pour une fonction d'erreur d'entropie relative.
 * @param n Référence du neurone de sortie dont on souhaite calculer l'erreur locale.
 * @param sortieDesiree La sortie désirée pour l'exemple considéré.
 * @param deltasW Référence permettant de stocker l'erreur locale calculée.
 * @return La contribution à l'erreur générale du réseau de neurones.
 */
double CorrectionPerteEntropieRelative(Neurone &n, double sortieDesiree, double &deltasW);

/**
 * @brief Calcule l'erreur générale d'un perceptron mono/multi-couche(s).
 * @param neuronesSortie La couche de neurones de sortie du perceptron.
 * @param sortiesDesirees Les sorties désirée pour chacun des neurones de sortie du perceptron.
 * @param utiliserQuadratique Indique si la fonction d'erreur quadratique (<b>true</b>) ou d'entropie relative (<b>false</b>) doit être utilisée.
 * @param utiliserSoftmax Indique si la fonction d'activation utilisée sur la couche de sortie est la fonction Softmax (<b>true</b>).
 * @return L'erreur globale du perceptron mono/multi-couche(s).
 */
double CalculerErreurGlobale(vector<Neurone> &neuronesSortie, vector<double> &sortiesDesirees, bool utiliserQuadratique=true, bool utiliserSoftmax=false);

/**
 * @brief Calcule la dérivée de l'erreur avec la fonction: \f$ \frac{grad2-grad1}{\epsilon} \f$ .
 *
 * Utilisé par la fonction <b>EvaluerDifferencesFinies</b> afin de calculer le gradient de l'erreur par la méthode des différences finies.
 *
 * @param grad1 La dérivée de l'erreur pour le poids modifié avec \f$ -\epsilon \f$.
 * @param grad2 La dérivée de l'erreur pour le poids modifié avec \f$ +\epsilon \f$.
 * @param epsilon Le pas de la dérivée \f$ \epsilon \f$.
 * @return La valeur calculée par la fonction de la différence centrale: \f$ \frac{grad2 - grad1}{2\epsilon} \f$ .
 */
double FonctionDifferenceCentrale(double grad1, double grad2, double epsilon);

#endif // FONCTIONERREUR_H
