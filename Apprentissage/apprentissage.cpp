﻿#include "apprentissage.h"

int ConvergencePerceptron(Neurone &n, vector<vector<double>> &entrees, vector<double> &sortiesDesirees, size_t nbIterations)
{
  size_t nbErreurs=(size_t) INFINITY;

  if(entrees.size()!=sortiesDesirees.size())
    return nbErreurs;

  for(size_t i=0; i<nbIterations && nbErreurs>0; i++)
  {
    nbErreurs=0;
    for(size_t j=0; j<entrees.size(); j++)
    {
      vector<double> exemple=entrees[j];

      double sortie=n.Evaluer(exemple);

      //Si sortie = 0 et incorrecte: ajout des entrées
      if(sortie==0 && sortiesDesirees[j]==1.0)
      {
        for(size_t k=0; k<n.poids.size(); k++)
            n.poids[k]+=exemple[k];

        nbErreurs++;
      } //Si sortie = 1 et incorrecte: soustraire les entrées
      else if (sortie==1 && sortiesDesirees[j]==0.0)
      {
        for(size_t k=0; k<n.poids.size(); k++)
        n.poids[k]+=exemple[k];

        nbErreurs++;
      } //Sinon ne rien faire
    }

    //cout << "Iteration " << i << "\tPoids: " << n.PoidsToString()  <<"\tNombre d'erreurs: " << nbErreurs << endl;
  }

  //cout << "Poids finaux: " << n.PoidsToString() << endl;

  return nbErreurs;
}

//Help of: http://sebastianraschka.com/Articles/2015_singlelayer_neurons.html#the-perceptron-learning-rule
int ReglePerceptron(Neurone &n, vector<vector<double>> &entrees, vector<double> &sortiesDesirees, size_t nbIterations, double tauxApprentissage)
{
  size_t nbErreurs=(size_t) INFINITY;

  // Algorithme d'apprentissage du perceptron:
  for(size_t i=0; i<nbIterations && nbErreurs>0; i++)
  {
    nbErreurs=0;
    //Présenter un vecteur d'entrée X et évaluer la sorties du neurone
    for(size_t j=0; j<entrees.size(); j++)
    {
      vector<double> exemple=entrees[j];
      double sortieTmp=n.Evaluer(exemple);

      // Calculer les corrections avec la variation du poids
      //  MAJ les poids: W(l+1) = w(l) + \eta (t-y)x_i : t=target output, l= indice d'itération, eta: gain/learning rate
      double correction = tauxApprentissage * (sortiesDesirees[j] - sortieTmp);
      for(size_t k=0; k<n.poids.size(); k++)
	n.poids[k] += correction * exemple[k];

      n.biais+=correction;

      if(sortieTmp!=sortiesDesirees[j])
	nbErreurs++;
    }

    cout << "Iteration " << i << "\tPoids: " << n.PoidsToString()  <<"\tNombre d'erreurs: " << nbErreurs << endl;
  }

  cout << "Poids finaux: " << n.PoidsToString() << endl;

  return nbErreurs;
}

int DescenteGradient(Neurone &n, vector<vector<double>> &entrees, vector<double> &sortiesDesirees, size_t nbIterations, double tauxApprentissage)
{
  size_t nbErreurs=(size_t) INFINITY;
  double erreur=0.0f;

  for(size_t i=0; i<nbIterations && nbErreurs>0; i++)
  {
    double deltaB=0.0f;
    nbErreurs=0;
    erreur=0.0f;
    vector<double> deltaPoids(n.poids.size()); // init à 0 dans le constructeur

    //Présenter un vecteur d'entrée X et évaluer la sorties du neurone
    for(size_t j=0; j<entrees.size(); j++)
    {
      vector<double> exemple=entrees[j];
      //pour chaque neurone
      double sortieTmp = n.Evaluer(exemple), //1. calculer ma sortie

      // Calculer les corrections avec la variation du poids
      //  MAJ les poids: W(l+1) = w(l) + \eta Somme((t-y)x_i) : t=target output, l= indice d'itération, eta: gain/learning rate
      diff = (sortiesDesirees[j] - n.potentiel);
      erreur += 0.5*pow(diff,2);
      deltaB += tauxApprentissage * diff;
      for(size_t k=0; k<deltaPoids.size(); k++)
	deltaPoids[k] += tauxApprentissage * diff * exemple[k];

       if(sortiesDesirees[j]!=sortieTmp)
	nbErreurs++;
    }

    //MAJ poids
    for(size_t k=0; k<n.poids.size(); k++)
      n.poids[k] += deltaPoids[k];

    n.biais += deltaB;

    cout << "Iteration " << i << "\tPoids: " << n.PoidsToString()  <<"\tNombre d'erreurs: " << nbErreurs
	 << "\tErreur moyenne: " << erreur/entrees.size() << endl;
  }

  //cout << "Poids finaux (+ biais) : " << n.PoidsToString() << endl;

  return nbErreurs;
}

//Help of: http://thebrain.mcgill.ca/flash/capsules/pdf_articles/reseau_neurones.pdf
double DescenteGradientWidrowHoff(Neurone &n, vector<vector<double>> &entrees, vector<double> &sortiesDesirees, size_t nbIterations, double tauxApprentissage, double seuilTolerance)
{
  size_t nbErreurs=(size_t) INFINITY;
  double erreurMoyenne=(double)entrees.size(); // Erreur Moyenne doit être plus petite que Seuil Tolerance: si paramètre par défaut seuilTolerance = INFINI

  /*if(seuilTolerance!=INFINITY) // Si le code client souhaite utiliser le seuil de tolérance
    erreurMoyenne = seuilTolerance+1;
  else // si le code client souhaite ne pas utiliser le seuil de tolérance
  {
    seuilTolerance
  }*/


  //for(size_t i=0; i<nbIterations /*&& nbErreurs>0*/ && seuilTolerance>erreurMoyenne; i++)
    for(size_t i=0; i<nbIterations && /*nbErreurs>0 &&*/ erreurMoyenne>seuilTolerance; i++)
  {
    nbErreurs=0;
    erreurMoyenne=0.0f;
    //Présenter un vecteur d'entrée X et évaluer la sorties du neurone
    for(size_t j=0; j<entrees.size(); j++)
    {
      vector<double> &exemple=entrees[j];
      //pour chaque neurone
      /*double sortieTmp=*/n.Evaluer(exemple); //1. calculer ma sortie
      double diff=tauxApprentissage * (sortiesDesirees[j] - n.potentiel) ; //2. calculer l'erreur entre prédit et non prédit
      erreurMoyenne += pow((sortiesDesirees[j] - n.potentiel),2); // Sinon erreur moyenne devient trop grande avec beaucoup d'exemples
      n.biais += diff;

      //  MAJ les poids: W(l+1) = w(l) + \eta Somme((t-y)x_i) : t=target output, l= indice d'itération, eta: gain/learning rate
      for(size_t k=0; k<n.poids.size(); k++)
        n.poids[k] += diff * exemple[k];

      //cout << "Sortie obtenue : " << sortieTmp << endl;

      /*if(sortiesDesirees[j]!=sortieTmp)
        nbErreurs++;*/
    }

    erreurMoyenne *= 0.5/entrees.size();
    //cout << "Iteration " << i << "\tErreur Moyenne: " << erreurMoyenne << "\t Nombre d'erreurs: " << nbErreurs << endl;

    /*cout << "Iteration " << i << "\tPoids: " << n.PoidsToString()  <<"\tNombre d'erreurs: " << nbErreurs
        << "\tErreur Moyenne : " << erreurMoyenne << endl;*/
  }

  //cout << "Poids finaux : " << n.PoidsToString() << endl;

  //return nbErreurs;
  return erreurMoyenne;
}

////////////////////////////////////////// RETROPROPAGATION DE LA DESCENTE DU GRADIENT
double RetropropagationGradient(ReseauNeurone &rdn, vector<vector<double>> &entrees, vector<vector<double>> &sortiesDesirees, size_t nbIterations, double tauxApprentissage, double seuilTolerance, bool utiliserQuadratique)
{
  vector<vector<double>> deltasW, deltasB;
  vector<vector<vector<double>>> deltasWij;
  double erreurMoyenne=INFINITY;
  size_t nbCouches=rdn.couches.size();

  // Initialisation du tableau de deltas W et B et Wij
  CreerTableauxDeltas(deltasW, deltasB, deltasWij, rdn);

  for(size_t i=0; i<nbIterations && abs(erreurMoyenne)>seuilTolerance; i++)
  {
    // Reinitialiser les accumulateurs à chaque début de batch
    InitialiserTableauxDeltas(deltasW, deltasB, deltasWij, nbCouches);
    erreurMoyenne=0.0f;
    for(size_t j=0; j<entrees.size(); j++)
    {
      // FORWARD PROPAGATION
      vector<double> exemple=entrees[j];
      rdn.Evaluer(exemple); // Présenter un vecteur d'entrée X et évaluer la sorties du neurone

      // BACK PROPAGATION
      erreurMoyenne += Retropropager(rdn, exemple, sortiesDesirees[j], deltasW, deltasWij, utiliserQuadratique);

      // Copier les Deltas W dans Deltas B
      for(size_t k=0; k<deltasW.size(); k++)
	       for(size_t l=0; l<deltasW[k].size(); l++)
	         deltasB[k][l] += deltasW[k][l];
    }

    if(entrees.size()>0)
      erreurMoyenne /= entrees.size();
    else
      erreurMoyenne /= 1;

    // MAJ poids après avoir parcouru l'ensemble des exemples
    MAJPoids(rdn, deltasB, deltasWij, tauxApprentissage);

    //cout << "Iteration " << i << '\t' << "Erreur Moyenne=" << erreurMoyenne << endl;
  }

  return erreurMoyenne;
}

double Retropropager(ReseauNeurone &rdn, vector<double> &exemple, vector<double> &sortiesDesirees, vector<vector<double>> &deltasW, vector<vector<vector<double>>> &deltasWij, bool utiliserQuadratique)
{
  size_t nbCouches=rdn.couches.size(), indiceCoucheSortie=nbCouches-1, correctionIndice=0;

  if(indiceCoucheSortie==0) // Si un réseau à un seul neurone
  {
    rdn.sortiesReseau[indiceCoucheSortie]=exemple;
    correctionIndice=1;
  }

  CalculerErreursCoucheSortie(rdn.couches[indiceCoucheSortie], rdn.sortiesReseau[indiceCoucheSortie-1+correctionIndice], sortiesDesirees,
      deltasW[indiceCoucheSortie], deltasWij[indiceCoucheSortie], utiliserQuadratique);

  // Calculer les erreurs des neurones cachés: d(l) = (w(l+1) * delta(l+1)) *  sigma'(z(l)) : l=indice couche, sigma=fct activation, sigma': derivée sigma
  for(int l=indiceCoucheSortie-1; l>0; l--) // l: indice couche (layer)-, l>-1 car la première couche du RDN est à l'indice 0
    CalculerErreursCoucheCachee(rdn.couches[l], rdn.couches[l+1], rdn.sortiesReseau[l-1], deltasW[l+1], deltasW[l], deltasWij[l]);

  // Calculer erreur couche d'entrée
  if(deltasW.size()>1) //Si RDN à 1 seul neurone, il n'y a qu'une seule couche
    CalculerErreursCoucheCachee(rdn.couches[0], rdn.couches[1], exemple, deltasW[1], deltasW[0], deltasWij[0]);

  double erreurGlobale = CalculerErreurGlobale(rdn.getCoucheSortie(), sortiesDesirees, utiliserQuadratique, rdn.isSoftmax);

  return erreurGlobale;
}

double CalculerErreursCoucheSortie(vector<Neurone> &src, vector<double> &entrees, vector<double> &sortieDesiree, vector<double> &deltasW, vector<vector<double>> &deltasWij, bool utiliserQuadratique)
{
  double erreurGlobale=0.0f;

  for(size_t i=0; i<deltasW.size(); i++) // Pour chaque neurone de la couche
  {
    if(utiliserQuadratique)
      erreurGlobale += CorrectionPerteQuadratique(src[i], sortieDesiree[i], deltasW[i]);
    else //sinon utiliser Entropie Relative
      erreurGlobale += CorrectionPerteEntropieRelative(src[i], sortieDesiree[i], deltasW[i]);

    for(size_t k=0; k<src[i].poids.size(); k++)
      deltasWij[i][k] += deltasW[i] * entrees[k];
  }

  return erreurGlobale;
}

void CalculerErreursCoucheCachee(vector<Neurone> &src, vector<Neurone> &coucheUlterieure, vector<double> &entrees, vector<double> &deltasCoucheUlterieure, vector<double> &deltasWDst, vector<vector<double>> &deltasWij)
{
  for(size_t i=0; i<src.size(); i++) // Pour chaque neurone de la couche actuelle
  {
    double errtmp = 0.0f;

    for(size_t j=0; j<coucheUlterieure.size(); j++) //Pour chaque neurone de la couche ultérieure
      //Calculer erreur avec: erreur * poids correspond a ce neurone, de la couche ultérieure
      errtmp += deltasCoucheUlterieure[j] * coucheUlterieure[j].poids[i];

    //erreur locale à ce neurone = errtmp * fction dérivée du neuronne
    deltasWDst[i] = errtmp * src[i].getDerivee();

    for(size_t k=0; k<src[i].poids.size(); k++)
      deltasWij[i][k] += deltasWDst[i] * entrees[k];
  }
}

void MAJPoids(ReseauNeurone &rdn, vector<vector<double>> &deltasW, vector<vector<vector<double>>> &deltasWij, double tauxApprentissage)
{
  size_t nbCouches=rdn.couches.size();

  // MAJ neurones couche cachée
  for(size_t l=0; l<nbCouches; l++) // l: indice couche (layer)
    MAJCoucheNeurones(rdn.couches[l], tauxApprentissage, deltasW[l], deltasWij[l]);
}

void MAJCoucheNeurones(vector<Neurone> &neurones, double tauxApprentissage, vector<double> &deltasB, vector<vector<double>> &deltasWij)
{
  for(size_t i=0; i<neurones.size(); i++) // Pour chaque neurone de la couche
  {
    for(size_t j=0; j<neurones[i].poids.size(); j++) // Pour chaque poids du neurone
      neurones[i].poids[j] += tauxApprentissage * deltasWij[i][j]; // MAJ POIDS

    neurones[i].biais += tauxApprentissage * deltasB[i]; // MAJ biais
  }
}

////////////// Fonctions utilitaires
void CreerTableauxDeltas(vector<vector<double>> &deltasW, vector<vector<double>> &deltasB, vector<vector<vector<double>>> &deltasWij, ReseauNeurone &rdn)
{
  size_t nbCouches=rdn.couches.size();
  deltasWij=vector<vector<vector<double>>>(nbCouches);
  deltasW=vector<vector<double>>(nbCouches);
  deltasB=vector<vector<double>>(nbCouches);
  for(size_t i=0; i<nbCouches; i++)
  {
    deltasW[i]=vector<double>(rdn.couches[i].size());// Autant de delta que de neurones dans la couche
    deltasB[i]=vector<double>(rdn.couches[i].size());

    vector<vector<double>> tmp(rdn.couches[i].size());
    for(size_t j=0; j<tmp.size(); j++)
      tmp[j]=vector<double>(rdn.couches[i][j].poids.size());

    deltasWij[i]=tmp;
  }
}

void InitialiserTableauxDeltas(vector<vector<double>> &deltasW, vector<vector<double>> &deltasB, vector<vector<vector<double>>> &deltasWij, size_t nbCouches)
{
  for(size_t i=0; i<nbCouches; i++)
  {
    for(size_t j=0; j<deltasWij[i].size(); j++)
    {
      for(size_t k=0; k<deltasWij[i][j].size(); k++)
	       deltasWij[i][j][k] = 0.0f;

      deltasB[i][j] = 0.0f;
      deltasW[i][j] = 0.0f;
    }
  }
}

void EvaluerDifferencesFinies(ReseauNeurone &rdn, vector<double> &exemple, vector<double> &sortiesDesirees,
			      vector<vector<double>> &deriveesBiais, vector<vector<vector<double>>> &deriveesPoids,
			      double epsilon, bool utiliserQuadratique)
{
  //Pour chaque poids de chaque neurone de chaque couche
  for(size_t i=0; i<rdn.couches.size(); i++)
  {
    for(size_t j=0; j<rdn.couches[i].size(); j++)
    {
      // Dérivé des poids
      for(size_t k=0; k<rdn.couches[i][j].poids.size(); k++)
      {
	double grad2=CalculerErreurPerturbation(rdn, &rdn.couches[i][j].poids[k], epsilon, exemple, sortiesDesirees, utiliserQuadratique);
	double grad1=CalculerErreurPerturbation(rdn, &rdn.couches[i][j].poids[k], -epsilon, exemple, sortiesDesirees, utiliserQuadratique);
	deriveesPoids[i][j][k] = FonctionDifferenceCentrale(grad1, grad2, epsilon);
      }

      // Dérivée du biais
      double grad2=CalculerErreurPerturbation(rdn, &rdn.couches[i][j].biais, epsilon, exemple, sortiesDesirees, utiliserQuadratique);
      double grad1=CalculerErreurPerturbation(rdn, &rdn.couches[i][j].biais, -epsilon, exemple, sortiesDesirees, utiliserQuadratique);
      deriveesBiais[i][j] = FonctionDifferenceCentrale(grad1, grad2, epsilon);
    }
  }
}

double CalculerErreurPerturbation(ReseauNeurone &rdn, double *poidsAModifier, double epsilon, vector<double> &exemple, vector<double> &sorties, bool utiliserQuadratique)
{
  size_t indiceCoucheSortie=rdn.couches.size()-1;
  vector<Neurone> &neuroneSortie=rdn.couches[indiceCoucheSortie];

  *poidsAModifier += epsilon; // Ajouter la perturbation au poids

  rdn.Evaluer(exemple); // Evaluer le RDN pour l'exemple
  double df=CalculerErreurGlobale(neuroneSortie, sorties, utiliserQuadratique, rdn.isSoftmax); // Calcule la dérivée de l'erreur

  *poidsAModifier -= epsilon; // Enlever la perturbation au poids

  return df;
}

void VerifierGradient(ReseauNeurone &rdn, vector<vector<double>> &entrees, vector<vector<double>> &sortiesDesirees, vector<vector<double>> &deltasB,
		      vector<vector<vector<double>>> &deltasWij, vector<vector<double>> &deriveesBiais, vector<vector<vector<double>>> &deriveesPoids,
		      double epsilon, bool utiliserQuadratique)
{
  double seuilPrecision = 1e-7;
  // Initialisation des tableaux des deltas (Backprop) et derivée (Difference Finies)
  CreerTableauxDeltas(deriveesBiais, deltasB, deltasWij, rdn);
  CreerTableauxDeltas(deriveesBiais, deltasB, deriveesPoids, rdn);
  size_t index = 99;
  cout << "indexe: " << index << endl;
  EvaluerDifferencesFinies(rdn, entrees[index], sortiesDesirees[index], deriveesBiais, deriveesPoids, epsilon, utiliserQuadratique);
  Retropropager(rdn, entrees[index], sortiesDesirees[index], deltasB, deltasWij, utiliserQuadratique);

  //Pour chaque poids de chaque neurone de chaque couche
  for(size_t i=0; i<rdn.couches.size(); i++)
  {
    for(size_t j=0; j<rdn.couches[i].size(); j++)
    {
      // Dérivé des poids
      for(size_t k=0; k<rdn.couches[i][j].poids.size(); k++)
      {
        cout << "C: " << i << " N: " << j << " P: " << k << " :: Delta: " << deltasWij[i][j][k] <<
            "\t Derivee: " << deriveesPoids[i][j][k] << endl;
        if(abs(deriveesPoids[i][j][k]-deltasWij[i][j][k])>seuilPrecision)
          cout << "!!! La derivee calculee et le delta du Poids ne respectent pas le seuil " <<
              seuilPrecision << " !!!" << endl;
      }
      cout << "C: " << i << " N: " << j << " BIAIS :: Delta: " << deltasB[i][j]
          << "\t Derivee: " << deriveesBiais[i][j] << endl;

      if(abs(deriveesBiais[i][j]-deltasB[i][j])>seuilPrecision)
        cout << "!!! La derivee calculee et le delta du Biais ne respectent pas le seuil " <<
            seuilPrecision << " !!!" << endl;
    }
  }
}

////////////////////////////////////////// DESCENTE DE GRADIENT STOCHASTIQUE
double DescenteGradientStochastique(ReseauNeurone &rdn, vector<vector<double>> &entrees, vector<vector<double>> &sortiesDesirees, size_t nbIterations, double tauxApprentissage, double seuilTolerance, bool utiliserQuadratique)
{
  vector<vector<double>> deltasW, deltasB;
  vector<vector<vector<double>>> deltasWij;
  vector<size_t> indexes; // Méthode où on parcourt l'ensemble des données qui a été mélangé
  double erreurMoyenne=INFINITY;
  size_t nbCouches=rdn.couches.size();

  // Initialisation du tableau de deltas W et B et Wij
  CreerTableauxDeltas(deltasW, deltasB, deltasWij, rdn);

  for(size_t i=0; i<nbIterations && abs(erreurMoyenne)>seuilTolerance; i++)
  {
    // Mélanger les données
    MelangerIndexes(entrees.size(), indexes);

    erreurMoyenne=0.0f;

    // Batch processing
    for(size_t j=0; j<entrees.size(); j++)
    {
      // Reinitialiser les accumulateurs pour chaque exemple
      InitialiserTableauxDeltas(deltasW, deltasB, deltasWij, nbCouches);

      // FORWARD PROPAGATION
      vector<double> exemple=entrees[indexes[j]];
      rdn.Evaluer(exemple); // Présenter un vecteur d'entrée X et évaluer la sorties du neurone

      // BACK PROPAGATION
      erreurMoyenne += Retropropager(rdn, exemple, sortiesDesirees[indexes[j]], deltasW, deltasWij, utiliserQuadratique);

      // MAJ poids à chaque exemple
      MAJPoids(rdn, deltasW, deltasWij, tauxApprentissage);
    }

    erreurMoyenne = erreurMoyenne / entrees.size();
    //cout << "Iteration " << i << '\t' << "Erreur Moyenne=" << erreurMoyenne << endl;
  }

  return erreurMoyenne;
}

double DescenteGradientStochastiqueMB(ReseauNeurone &rdn, vector<vector<double>> &entrees, vector<vector<double>> &sortiesDesirees, size_t nbIterations, size_t tailleMiniBatch, double tauxApprentissage, double seuilTolerance, bool utiliserQuadratique)
{
  vector<vector<double>> deltasW, deltasB;
  vector<vector<vector<double>>> deltasWij;
  vector<size_t> indexes; // Méthode où l'ensemble des données est parcourue qui a été mélangé
  double erreurMoyenne=INFINITY;
  size_t nbCouches=rdn.couches.size(),
          /* Centrer l'apprentissage sur le milieu du dataset. L'apprentissage va aller de :
	 [(Taille Dataset /2) - (Taille Minibatch /2 )  ; (Taille Dataset /2) + (Taille Minibatch /2 )] */
      milieuDataset=(entrees.size()/2)-(tailleMiniBatch/2);

  // Initialisation du tableau de deltas W et B et Wij
  CreerTableauxDeltas(deltasW, deltasB, deltasWij, rdn);

  for(size_t i=0; i<nbIterations && abs(erreurMoyenne)>seuilTolerance; i++)
  {
    // Mélanger les données
    MelangerIndexes(entrees.size(), indexes);

    // Reinitialiser les accumulateurs pour chaque MiniBatch
    InitialiserTableauxDeltas(deltasW, deltasB, deltasWij, nbCouches);

    erreurMoyenne=0.0f;
    for(size_t j=0; j<tailleMiniBatch && !isnan(erreurMoyenne); j++)
    {
      size_t indexeActuel=indexes[milieuDataset+j];
      // FORWARD PROPAGATION
      vector<double> exemple=entrees[indexeActuel];
      rdn.Evaluer(exemple); // Présenter un vecteur d'entrée X et évaluer la sorties du neurone

      // BACK PROPAGATION
      erreurMoyenne += Retropropager(rdn, exemple, sortiesDesirees[indexeActuel], deltasW, deltasWij, utiliserQuadratique)/tailleMiniBatch;
    }

    // MAJ poids à chaque fin de MiniBatch
    MAJPoids(rdn, deltasW, deltasWij, tauxApprentissage);

    //erreurMoyenne = (double) (erreurMoyenne / tailleMiniBatch);
    //cout << "Iteration " << i << '\t' << "Erreur Moyenne=" << erreurMoyenne << endl;
  }

  return erreurMoyenne;
}

//// USELESS
size_t GenererNombreAleatoire(vector<size_t> &nombreDejaUtilise, size_t NombreIterationMax, size_t tailleDonnees)
{
  size_t indiceHasard=0;
  for(size_t i=0; i<NombreIterationMax; i++)
  {
    srand(i+time(NULL));
    indiceHasard = rand() % tailleDonnees;

    if(std::find(nombreDejaUtilise.begin(), nombreDejaUtilise.end(), indiceHasard)
       == nombreDejaUtilise.end())
    { // Si élément n'a pas été trouvé, l'ajouter à la liste et quitter la boucle
      nombreDejaUtilise.push_back(indiceHasard);
      break;
    }
  }

  return indiceHasard;
}

void MelangerIndexes(size_t nombreDonnees, vector<size_t> &indexesDst)
{
  std::random_device rng;
  std::mt19937 urng(rng());

  // Créer un vecteur contenant les indexes des deux vecteurs entrees et sortieDesirees
  vector<size_t> indexes(nombreDonnees);
  for(size_t i=0; i<indexes.size(); i++)
      indexes[i]=i;

  //Mélanger les indexes
  //random_shuffle(indexes.begin(), indexes.end()); // deprecated
  shuffle(indexes.begin(), indexes.end(), urng);

  indexesDst=indexes;
}
