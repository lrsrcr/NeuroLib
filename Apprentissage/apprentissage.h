﻿#ifndef APPRENTISSAGE_H
#define APPRENTISSAGE_H

/** @file apprentissage.h
 * @brief Contient l'ensemble des algorithmes de descente de gradient pour les perceptrons mono et multi-couche(s).
 * @author DI TROIA J. , BONNIN P.
 * @version 1.0
 * @date 01 août 2018
 */

#include <iostream>

#include <Neurone/reseauneurone.h>
#include <Fonctions/fonctionerreur.h>

using namespace std;

/**
 * @brief Implémentation de l'algorithme de convergence d'un perceptron <b>mono-couche</b>.
 *
 * L'algorihme s'arrête s'il n'y a plus d'erreur de classification des données passées en paramètre ou si le nombre d'itérations <b>nbIterations</b> a été atteint.
 *
 * @param n Le perceptron devant converger.
 * @param entrees Les exemples disponibles pour entraîner le perceptron n.
 * @param sortiesDesirees Les réponses correspondant aux exemples disponibles.
 * @param nbIterations Le nombre d'itérations maximum de l'algorithme de convergence.
 * @return Le nombre d'erreur de classification des entrées fournies en paramètre restant après apprentissage.
 */
int ConvergencePerceptron(Neurone &n, vector<vector<double>> &entrees, vector<double> &sortiesDesirees, size_t nbIterations=5);

/**
 * @brief Implémentation de la règle d'apprentissage d'un perceptron <b>mono-couche</b>.
 *
 * L'algorihme s'arrête s'il n'y a plus d'erreur de classification des données passées en paramètre ou si le nombre d'itérations <b>nbIterations</b> a été atteint.
 *
 * @param n Le perceptron devant converger.
 * @param entrees Les exemples disponibles pour entraîner le perceptron n.
 * @param sortiesDesirees Les réponses correspondant aux exemples disponibles.
 * @param nbIterations Le nombre d'itérations maximum de l'algorithme de convergence.
 * @param tauxApprentissage Facteur appliqué à la correction qui est soustraite à chacun des poids.
 * @return Le nombre d'erreur de classification des entrées fournies en paramètre restant après apprentissage.
 */
int ReglePerceptron(Neurone &n, vector<vector<double>> &entrees, vector<double> &sortiesDesirees, size_t nbIterations=5, double tauxApprentissage=0.05);

/**
 * @brief Implémentation de la descente de gradient pour un perceptron <b>mono-couche</b>.
 *
 * L'algorihme s'arrête s'il n'y a plus d'erreur de classification des données passées en paramètre ou si le nombre d'itérations <b>nbIterations</b> a été atteint.
 *
 * @param n Le perceptron devant converger.
 * @param entrees Les exemples disponibles pour entraîner le perceptron n.
 * @param sortiesDesirees Les réponses correspondant aux exemples disponibles.
 * @param nbIterations Le nombre d'itérations maximum de l'algorithme de convergence.
 * @param tauxApprentissage Facteur appliqué à la correction qui est soustraite à chacun des poids.
 * @return Le nombre d'erreur de classification des entrées fournies en paramètre restant après apprentissage.
 */
int DescenteGradient(Neurone &n, vector<vector<double> > &entrees, vector<double> &sortiesDesirees, size_t nbIterations=5, double tauxApprentissage=0.05);
//ADALINE: effectue la correction avant le seuil contrairement au Perceptron
/**
 * @brief Implémentation de la descente de gradient pour un perceptron <b>mono-couche</b>.
 *
 * L'algorihme s'arrête si l'erreur moyenne calculée est inférieure au seuil de tolérance contenu dans le paramètre <b>seuilTolerance</b>.
 * De plus, pour fonctionner correctement, le perceptron <b>n</b> doit utiliser une fonction d'activation <b>continue</b>.
 *
 * @param n Le perceptron devant converger.
 * @param entrees Les exemples disponibles pour entraîner le perceptron n.
 * @param sortiesDesirees Les réponses correspondant aux exemples disponibles.
 * @param nbIterations Le nombre d'itérations maximum de l'algorithme de convergence.
 * @param tauxApprentissage Facteur appliqué à la correction qui est soustraite à chacun des poids.
 * @param seuilTolerance Valeur comparée à l'erreur moyenne. Si l'erreur moyenne est inférieur à cette valeur, alors l'algorithme s'arrête.
 * @return L'erreur moyenne du perceptron <b>mono-couche</b> calculée à partir des réponses fournies par ce perceptron comparée avec les valeurs attendues.
 */
double DescenteGradientWidrowHoff(Neurone &n, vector<vector<double> > &entrees, vector<double> &sortiesDesirees, size_t nbIterations=5, double tauxApprentissage=0.01, double seuilTolerance=INFINITY);

// RETROPROPAGATION DE LA DESCENTE DU GRADIENT
/**
 * @brief Applique la descente de gradient Full-Batch au réseau de neurones <b>rdn</b>.
 * @param rdn Le perceptron <b>multi-couches</b>.
 * @param entrees Les exemples disponibles pour entraîner le perceptron n.
 * @param sortiesDesirees Les réponses correspondant aux exemples disponibles.
 * @param nbIterations Le nombre d'itérations maximum de l'algorithme de convergence.
 * @param tauxApprentissage Facteur appliqué à la correction qui est soustraite à chacun des poids.
 * @param seuilTolerance Valeur comparée à l'erreur moyenne. Si l'erreur moyenne est inférieur à cette valeur, alors l'algorithme s'arrête.
 * @param utiliserQuadratique Booléen indiquant si la fonction d'erreur utilisée est la quadratique (<b>true</b>) ou celle de l'entropie croisée (<b>false</b>).
 * @return L'erreur moyenne du perceptron <b>multi-couches</b> calculée à partir des réponses fournies par ce perceptron comparée avec les valeurs attendues.
 */
double RetropropagationGradient(ReseauNeurone &rdn, vector<vector<double> > &entrees, vector<vector<double> > &sortiesDesirees, size_t nbIterations=5, double tauxApprentissage=0.01f,
			     double seuilTolerance=0.05f, bool utiliserQuadratique=true);
/**
 * @brief Calcule les erreurs et corrections des différentes couches du réseau de neurones pour un seul exemple.
 * @param rdn Le perceptron <b>multi-couches</b> devant .
 * @param exemple L'exemple en cours d'analyse.
 * @param sortiesDesirees Les réponses correspondant aux exemples disponibles.
 * @param deltasW Matrice contenant les erreurs locales des différents neurones des différentes couches.
 * @param deltasWij Matrice contenant les corrections devant être appliquées aux poids des neurones du réseau.
 * @param utiliserQuadratique Booléen indiquant si la fonction d'erreur utilisée est la quadratique (<b>true</b>) ou celle de l'entropie croisée (<b>false</b>).
 * @return L'erreur du réseau de neurones pour cet exemple.
 */
double Retropropager(ReseauNeurone &rdn, vector<double> &exemple, vector<double> &sortiesDesirees, vector<vector<double>> &deltasW, vector<vector<vector<double>>> &deltasWij, bool utiliserQuadratique=true);

// Calculer les erreurs
/**
 * @brief Calcule les erreurs locales et les corrections à apporter aux poids des neurones de la couche de sortie.
 *
 * Cette fonction est appelée par la fonction <b>Retropropager</b>. Elle n'est pas destinée à être appelée par du code client.
 *
 * @param src La couche de sortie du réseau de neurones.
 * @param entrees L'exemple en cours d'analyse.
 * @param sortieDesiree Les réponses correspondant aux exemples disponibles.
 * @param deltasW Matrice contenant les erreurs locales des différents neurones des différentes couches.
 * @param deltasWij Matrice contenant les corrections devant être appliquées aux poids des neurones du réseau.
 * @param utiliserQuadratique Booléen indiquant si la fonction d'erreur utilisée est la quadratique (<b>true</b>) ou celle de l'entropie croisée (<b>false</b>).
 * @return L'erreur du réseau de neurones pour cet exemple.
 */
double CalculerErreursCoucheSortie(vector<Neurone> &src, vector<double> &entrees, vector<double> &sortieDesiree, vector<double> &deltasW, vector<vector<double>> &deltasWij, bool utiliserQuadratique=true);
/**
 * @brief Calcule les erreurs locales et les corrections à apporter aux poids des neurones de la couche de sortie.
 *
 * Cette fonction est appelée par la fonction <b>Retropropager</b>. Elle n'est pas destinée à être appelée par du code client.
 *
 * @param coucheNeurone La couche cachée du réseau actuellement en cours d'analyse.
 * @param coucheUlterieure La couche suivante (en direction de la sortie du réseau) de la couche actuellement en cours d'analyse <b>coucheNeurone</b>.
 * @param entrees L'exemple en cours d'analyse.
 * @param deltasCoucheUlterieure Les erreurs locales des neurones de la couche <b>coucheUlterieure</b>.
 * @param deltasWDst Vecteur modifié par la fonction et qui contiendra alors les erreurs locales de la couche actuelle <b>coucheNeurone</b>.
 * @param deltasWij Tableau contenant les corrections à appliquer aux poids de la couche actuelle <b>coucheNeurone</b>.
 */
void CalculerErreursCoucheCachee(vector<Neurone> &coucheNeurone, vector<Neurone> &coucheUlterieure, vector<double> &entrees, vector<double> &deltasCoucheUlterieure, vector<double> &deltasWDst, vector<vector<double> > &deltasWij);

// MAJ les poids
/**
 * @brief MAJPoids Met à jour les poids du réseau de neurones passé en paramètre.
 *
 * Cette fonction est appelée par la fonction <b>RetropropagationGradient</b>. Elle n'est pas destinée à être appelée par du code client.
 *
 * @param rdn Le réseau de neurones à mettre à jour.
 * @param deltasW Les erreurs locales des neurones du réseaux.
 * @param deltasWij Les corrections à appliquer aux poids du réseau.
 * @param tauxApprentissage Le taux d'apprentissage appliqué aux corrections contenues dans <b>deltasWij</b>.
 */
void MAJPoids(ReseauNeurone & rdn, vector<vector<double>> &deltasW, vector<vector<vector<double> > > &deltasWij, double tauxApprentissage);
/**
 * @brief MAJCoucheNeurones
 *
 * Cette fonction est appelée par la fonction <b>MAJPoids</b>. Elle n'est pas destinée à être appelée par du code client.
 *
 * @param src Couche de neurones à mettre à jour.
 * @param tauxApprentissage Le taux d'apprentissage (facteur) à appliquer aux corrections des poids.
 * @param deltasW Les erreurs locales des neurones du réseaux.
 * @param deltasWij Les corrections à appliquer aux poids du réseau.
 */
void MAJCoucheNeurones(vector<Neurone> &src, double tauxApprentissage, vector<double> &deltasW, vector<vector<double> > &deltasWij);

// Fonctions utilitaires de la Rétropropagation du Gradient
/**
 * @brief Alloue l'espace nécessaire en mémoire des tableaux contenant les corrections et les erreurs locales.
 *
 * Cette fonction est appelée par les fonctions de descente de gradient. Elle n'est pas destinée à être appelée par du code client.
 *
 * @param deltasW Les erreurs locales des neurones du réseaux.
 * @param deltasB Les corrections à appliquer aux biais des neurones du réseau.
 * @param deltasWij Les corrections à appliquer aux poids du réseau.
 * @param rdn Le perceptron <b>multi-couches</b> auquel sera appliquer la descente de gradient.
 */
void CreerTableauxDeltas(vector<vector<double>> &deltasW, vector<vector<double> > &deltasB, vector<vector<vector<double>>> & deltasWij, ReseauNeurone &rdn);
/**
 * @brief Met à 0 l'ensemble des éléments des différents tableaux, mais ne redimensionne pas les tableaux.
 *
 * Cette fonction est appelée par les fonctions de descente de gradient à chaque début d'itération. Elle n'est pas destinée à être appelée par du code client.
 *
 * @param deltasW Tableau contenant les erreurs locales des neurones du réseaux.
 * @param deltasB Tableau contenant les corrections à appliquer aux biais des neurones du réseau.
 * @param deltasWij Tableau contenant les corrections à appliquer aux poids du réseau.
 * @param nbCouches
 */
void InitialiserTableauxDeltas(vector<vector<double>> &deltasW, vector<vector<double> > &deltasB, vector<vector<vector<double>>> & deltasWij, size_t nbCouches);

// Vérifier les erreurs
/**
 * @brief Calcule les corrections à appliquer aux réseaux de neurones avec la méthode des Différences Finies.
 *
 * Cette méthode est nettement plus lente que celle de la descente de gradient car elle calcule les corrections en appliquant le calcule d'une dérivée avec la fonction de la différence centrale.
 * Elle permet de vérifier l'implémentation d'un algorithme de descente de gradient et n'est pas destinée a être appelée dans du code client.
 *
 * @param rdn Le perceptron <b>multi-couches</b> auquel sera appliquée la méthode des différences finies.
 * @param entrees Les exemples disponibles pour entraîner le perceptron n.
 * @param sortiesDesirees Les réponses correspondant aux exemples disponibles.
 * @param deriveesBiais Tableau contenant les corrections à appliquer aux biais des neurones du réseau.
 * @param deriveesPoids Tableau contenant les corrections à appliquer aux poids du réseau.
 * @param epsilon Valeur du pas de la dérivée.
 * @param utiliserQuadratique Booléen indiquant si la fonction d'erreur utilisée est la quadratique (<b>true</b>) ou celle de l'entropie croisée (<b>false</b>).
 */
void EvaluerDifferencesFinies(ReseauNeurone &rdn, vector<double> &entrees, vector<double> &sortiesDesirees, vector<vector<double>> &deriveesBiais,
			      vector<vector<vector<double>>> &deriveesPoids, double epsilon=1e-4, bool utiliserQuadratique=true);
/**
 * @brief Calcul la dérivée de l'erreur du réseau en perturbant le poids <b>poidsAModifier</b>.
 *
 * Cette fonction est utilisée par la fonction <b>EvaluerDifferencesFinies</b> et elle n'est pas destinée a être appelée dans du code client.
 *
 * @param rdn Le perceptron <b>multi-couches</b> auquel sera appliquée la méthode des différences finies.
 * @param poidsAModifier Pointeur vers le poids dont on souhaite calculer la correction.
 * @param epsilon Valeur du pas de la dérivée.
 * @param exemple L'exemple en cours d'analyse.
 * @param sorties Les réponses correspondant à l'exemple en cours d'analyse.
 * @param utiliserQuadratique Booléen indiquant si la fonction d'erreur utilisée est la quadratique (<b>true</b>) ou celle de l'entropie croisée (<b>false</b>).
 * @return La perturbation du poids.
 */
double CalculerErreurPerturbation(ReseauNeurone &rdn, double *poidsAModifier, double epsilon, vector<double> &exemple, vector<double> &sorties, bool utiliserQuadratique=false);
/**
 * @brief Vérifie l'implémentation d'une descente de gradient en calculant les corrections à appliquer aux poids en fonction d'un exemple au hasard.
 *
 * Calcule les corrections à appliquer aux poids d'un réseau de neurones suivant les deux méthodes: la descente de gradient et la méthode des Différences Finies.
 * Cette fonction n'est pas destinée à être appelée dans du code client.
 *
 * @param rdn Le perceptron <b>multi-couches</b> auquel sera appliquée la méthode des différences finies et la descente de gradient.
 * @param entrees Les exemples disponibles pour entraîner le perceptron n.
 * @param sortiesDesirees Les réponses correspondant aux exemples disponibles.
 * @param deltasB Les corrections à appliquer aux biais des neurones du réseau calculées par la descente de gradient.
 * @param deltasWij Les corrections à appliquer aux poids du réseau calculées par la descente de gradient.
 * @param deriveesBiais Tableau contenant les corrections à appliquer aux biais des neurones du réseau  calculées par la méthode des Différences Finies.
 * @param deriveesPoids Tableau contenant les corrections à appliquer aux poids du réseau calculées par la méthode des Différences Finies.
 * @param epsilon Valeur du pas de la dérivée.
 * @param utiliserQuadratique Booléen indiquant si la fonction d'erreur utilisée est la quadratique (<b>true</b>) ou celle de l'entropie croisée (<b>false</b>).
 */
void VerifierGradient(ReseauNeurone &rdn, vector<vector<double>> &entrees, vector<vector<double>> &sortiesDesirees, vector<vector<double>> &deltasB,
		      vector<vector<vector<double>>> &deltasWij, vector<vector<double>> &deriveesBiais, vector<vector<vector<double>>> &deriveesPoids,
		      double epsilon=1e-4, bool utiliserQuadratique=true);

// DESCENTE DE GRADIENT STOCHASTIQUE (SGD) & Mini-Batch (MB)
/**
 * @brief Applique la descente de gradient Stochastique (SGD) au réseau de neurones <b>rdn</b>.
 * @param rdn Le perceptron <b>multi-couches</b>.
 * @param entrees Les exemples disponibles pour entraîner le perceptron n.
 * @param sortiesDesirees Les réponses correspondant aux exemples disponibles.
 * @param nbIterations Le nombre d'itérations maximum de l'algorithme de convergence.
 * @param tauxApprentissage Facteur appliqué à la correction qui est soustraite à chacun des poids.
 * @param seuilTolerance Valeur comparée à l'erreur moyenne. Si l'erreur moyenne est inférieur à cette valeur, alors l'algorithme s'arrête.
 * @param utiliserQuadratique Booléen indiquant si la fonction d'erreur utilisée est la quadratique (<b>true</b>) ou celle de l'entropie croisée (<b>false</b>).
 * @return L'erreur moyenne du perceptron <b>multi-couches</b> calculée à partir des réponses fournies par ce perceptron comparée avec les valeurs attendues.
 */
double DescenteGradientStochastique(ReseauNeurone &rdn, vector<vector<double> > &entrees, vector<vector<double> > &sortiesDesirees, size_t nbIterations=5, double tauxApprentissage=0.01f, double seuilTolerance=0.05f, bool utiliserQuadratique=true);
/**
 * @brief Applique la descente de gradient Mini-Batch au réseau de neurones <b>rdn</b>.
 * @param rdn Le perceptron <b>multi-couches</b>.
 * @param entrees Les exemples disponibles pour entraîner le perceptron n.
 * @param sortiesDesirees Les réponses correspondant aux exemples disponibles.
 * @param nbIterations Le nombre d'itérations maximum de l'algorithme de convergence.
 * @param tailleMiniBatch Taille du batch utlisé lors de l'exécution de la descente de gradient.
 * @param tauxApprentissage Facteur appliqué à la correction qui est soustraite à chacun des poids.
 * @param seuilTolerance Valeur comparée à l'erreur moyenne. Si l'erreur moyenne est inférieur à cette valeur, alors l'algorithme s'arrête.
 * @param utiliserQuadratique Booléen indiquant si la fonction d'erreur utilisée est la quadratique (<b>true</b>) ou celle de l'entropie croisée (<b>false</b>).
 * @return L'erreur moyenne du perceptron <b>multi-couches</b> calculée à partir des réponses fournies par ce perceptron comparée avec les valeurs attendues.
 */
double DescenteGradientStochastiqueMB(ReseauNeurone &rdn, vector<vector<double>> &entrees, vector<vector<double>> &sortiesDesirees, size_t nbIterations=5, size_t tailleMiniBatch=10, double tauxApprentissage=0.01f, double seuilTolerance=0.05f, bool utiliserQuadratique=true);

// Fonction utilitaire de la Descente de Gradient Stochastique
/**
 * @brief Fonction inutile.
 * @param nombreDejaUtilise
 * @param NombreIterationMax
 * @param tailleDonnees
 * @return
 */
size_t GenererNombreAleatoire(vector<size_t> &nombreDejaUtilise, size_t NombreIterationMax, size_t tailleDonnees);
/**
 * @brief Mélange les indexes des données afin de pouvoir sélectionner des données de façon aléatoire.
 *
 * Fonction utilisée par la fonction <b>DescenteGradientStochastique</b>.
 *
 * @param nombreDonnees Quantité de données disponibles.
 * @param indexesDst Vecteur où seront stocké les indexes des données mélangés.
 */
void MelangerIndexes(size_t nombreDonnees, vector<size_t> &indexesDst);

#endif // APPRENTISSAGE_H
